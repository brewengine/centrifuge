package com.brewengine.centrifuge;

import android.os.Bundle;

import com.brewengine.centrifuge.common.CentrifugeAndroidCommon;
import com.flurry.android.FlurryAgent;

public class CentrifugeAndroidGame extends CentrifugeAndroidCommon {
	
	static final String FLURRY_KEY = "ICY1VUEEESRH4AY27GW7";
	
	/**
	 * Do not remove this method (needed by ProGuard).
	 * 
	 * Without this method ProGuard errors with:
	 * <pre>
	 * Warning: com.brewengine.centrifuge.CentrifugeAndroidGame$1: can't find enclosing method 'void onCreate(android.os.Bundle)' in class com.brewengine.centrifuge.CentrifugeAndroidGame
	 * </pre>
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public void onStart() {
       super.onStart();
       FlurryAgent.onStartSession(this, FLURRY_KEY);
    }
	
	@Override
	public void onStop() {
	   super.onStop();
	   FlurryAgent.onEndSession(this);
	}

	@Override
	public boolean isLite() {
		return false;
	}


}
