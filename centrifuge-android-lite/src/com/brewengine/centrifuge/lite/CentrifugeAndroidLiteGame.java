package com.brewengine.centrifuge.lite;

import com.brewengine.centrifuge.common.CentrifugeAndroidCommon;
import com.flurry.android.FlurryAgent;

public class CentrifugeAndroidLiteGame extends CentrifugeAndroidCommon {
	
	static final String FLURRY_KEY = "GBJRKDNMI9QLMQ2WVXAJ";

	@Override
	public void onStart() {
       super.onStart();
       FlurryAgent.onStartSession(this, FLURRY_KEY);
    }
	
	@Override
	public void onStop() {
	   super.onStop();
	   FlurryAgent.onEndSession(this);
	}
	
	@Override
	public boolean isLite() {
		return true;
	}
	
}