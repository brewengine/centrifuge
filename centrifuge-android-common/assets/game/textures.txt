
textures1.png
format: RGBA8888
filter: Nearest,Nearest
repeat: none
text
  rotate: false
  xy: 0, 0
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
x
  rotate: false
  xy: 0, 129
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
orientationRight
  rotate: false
  xy: 129, 0
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
orientationNormal
  rotate: false
  xy: 129, 79
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
orientationLeft
  rotate: false
  xy: 129, 158
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
orientationFlat2
  rotate: false
  xy: 258, 0
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
orientationFlat1
  rotate: false
  xy: 258, 79
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
orientationFlat0
  rotate: false
  xy: 258, 158
  size: 128, 78
  orig: 128, 78
  offset: 0, 0
  index: -1
openfeint
  rotate: false
  xy: 387, 0
  size: 75, 75
  orig: 75, 75
  offset: 0, 0
  index: -1
particle
  rotate: false
  xy: 0, 208
  size: 16, 16
  orig: 16, 16
  offset: 0, 0
  index: -1
goal
  rotate: false
  xy: 387, 79
  size: 13, 99
  orig: 13, 99
  offset: 0, 0
  index: -1
glow
  rotate: false
  xy: 0, 225
  size: 8, 8
  orig: 8, 8
  offset: 0, 0
  index: -1
