package com.brewengine.centrifuge.common;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.widget.Toast;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.actors.Label;
import com.brewengine.centrifuge.AssetsGame;
import com.brewengine.centrifuge.helpers.ImageHelper;
import com.brewengine.centrifuge.screens.LevelSelectorScreen;
import com.openfeint.api.OpenFeint;
import com.openfeint.api.OpenFeintDelegate;
import com.openfeint.api.OpenFeintSettings;
import com.openfeint.api.resource.Achievement;
import com.openfeint.api.resource.CurrentUser;
import com.openfeint.api.resource.Leaderboard;
import com.openfeint.api.resource.Leaderboard.GetScoresCB;
import com.openfeint.api.resource.Leaderboard.GetUserScoreCB;
import com.openfeint.api.resource.Score;
import com.openfeint.api.resource.User;
import com.openfeint.api.resource.User.DownloadProfilePictureCB;
import com.openfeint.internal.OpenFeintInternal;

public class OpenFeintHelper
{
	private static final String gameID = "360543";
	private static final String gameKey = "6pEsquuhFKbWA1QV5uvtVQ";
	private static final String gameSecret = "YdihoblGTXIGt3DLYdBmAwviVG1hEqwyt8tGjFdQ";
	
	public static final String[] LEVEL_LEADERBOARD = {
		"906117", "915497", // 1,2
		"915507", "917527", // 4
		"919407", "919417", // 6
		"925607", "919427", // 8
		"919437", "919447", // 10
		// 11 - 20
		};
	public static final String BEST_STREAK = "935006";
	public static final String CUMULATIVE_SCORE = "935026";
	public static final String SCORE_ZERO = "1276202";
	public static final String NO_HITTER = "1276232";
	public static final String BREAKER_BREAKER = "1276242";
	private static final String APPRECIATION_AWARD = "1276262";
	private static final String ACHIEVEMENT_720 = "1276222";
	private static final String PLAY_ALL_LEVELS = "1276212";
	private static final String OUT_OF_SIGHT = "1276252";
	
	private boolean openFeintResyncNeeded = true;
	private Activity activity;
	protected boolean invalidated; // accessed from activity
	private CLevel[] levels = new CLevel[LEVEL_LEADERBOARD.length];
	private HashMap<String, CUser> users = new HashMap<String, OpenFeintHelper.CUser>();
	private boolean posted720 = false;
    
	public OpenFeintHelper(Activity pActivity)
	{
		activity = pActivity;
	}
	
	public void postStreak(int streak)
	{
		Score s = new Score(streak);
		s.submitTo(new Leaderboard(BEST_STREAK), null);
	}
	
	public void postNoHitter()
	{
		new Achievement(NO_HITTER).unlock(null);
	}
	
    public void postScore(int level, final long scoreValue)
    {
    	if ( scoreValue == 0 )
    	{
    		Achievement a = new Achievement(SCORE_ZERO);
    		a.unlock(null);
    	}
    	
    	Score s = new Score(scoreValue, null); // Second parameter is null to indicate that custom display text is not used.
    	Leaderboard l = new Leaderboard(LEVEL_LEADERBOARD[level - 1]);
    	
    	s.submitTo(l, new Score.SubmitToCB() {

			@Override public void onSuccess(boolean newHighScore) {
    			// sweet, score was posted
//    			OpenFeintHelper.this.setResult(Activity.RESULT_OK);
//    			OpenFeintHelper.this.finish();
				openFeintResyncNeeded = true;
    		}

    		@Override public void onFailure(String exceptionMessage) {
    			Toast.makeText(activity,
    			"Error (" + exceptionMessage + ") posting score.",
    			Toast.LENGTH_SHORT).show();
//    			OpenFeintHelper.this.setResult(Activity.RESULT_CANCELED);
//    			OpenFeintHelper.this.finish();
    		}
    	});
    	
    	CurrentUser currentUser = OpenFeint.getCurrentUser();
    	if (currentUser != null) {
    		final Leaderboard cumulativeScore = new Leaderboard(CUMULATIVE_SCORE);
			cumulativeScore.getUserScore(currentUser, new GetUserScoreCB()
			{
				
				@Override
				public void onSuccess(Score score)
				{
					long previousScore = 0;
					if (score != null) {
						previousScore = score.score;
					}
					
					Score cumScore = new Score(previousScore + scoreValue);
					cumScore.submitTo(cumulativeScore, new Score.SubmitToCB()
					{
						
						@Override public void onSuccess(boolean newHighScore)
						{
							openFeintResyncNeeded = true;
						}
						@Override public void onFailure(String exceptionMessage) {
			    			Toast.makeText(activity,
			    			"Error (" + exceptionMessage + ") posting score.",
			    			Toast.LENGTH_SHORT).show();
			    		}
					});
				}
	    		@Override 
	    		public void onFailure(String exceptionMessage) {
	    			Toast.makeText(activity,
	    			"Error (" + exceptionMessage + ") posting score.",
	    			Toast.LENGTH_SHORT).show();
	    		}
			});
    	}
    	
    	int levelsCompleted = 0; 
    	
    	for (String loopLevel : LEVEL_LEADERBOARD)
		{
			Leaderboard leaderboard = new Leaderboard(loopLevel);
			Score localUserScore = leaderboard.localUserScore;
			if ( localUserScore != null && localUserScore.score != 0 )
			{
				levelsCompleted++;
			}
		}
    	
    	new Achievement(PLAY_ALL_LEVELS).updateProgression(levelsCompleted * LevelSelectorScreen.LEVEL_COUNT / 100, null);
    }

	public void getLevelScore(int level, Label scoreLabel, Label friendScoreLabel, Image friendImage)
	{
		if ( level > LEVEL_LEADERBOARD.length )
		{
			System.out.println("OpenFeintHelper.getLevelScore: Leaderboard not setup in OpenFeint!");
			return;
		}
		
		if ( openFeintResyncNeeded )
		{
			openFeintResyncNeeded = false;
			refreshScores(level, scoreLabel, friendScoreLabel, friendImage);
		}
		else
		{
			updateLevelUI(level, scoreLabel, friendScoreLabel, friendImage);
		}
	}

	private void updateLevelUI(int level, Object scoreLabel,
			Object friendScoreLabel, Image friendImage)
	{
		CLevel cLevel = levels[level-1];
		
		if ( cLevel == null )
		{
			return;
		}
		
		((Label)scoreLabel).setText( LevelSelectorScreen.BEST_SCORE + cLevel.personalBestScore );
		
		if ( cLevel.userWithBestScore != null )
		{
			((Label)friendScoreLabel).setText( LevelSelectorScreen.FRIEND_SCORE + cLevel.bestScoreFromSelfAndFriends
					+ " (" + cLevel.userWithBestScore.user.name + ")");
	
			
			if (cLevel.userWithBestScore.avatar == null) 
			{
				if ( cLevel.userWithBestScore.byteArray == null )
				{
					friendImage.region.setRegion(AssetsGame.getInstance().openfeintRegion);
					
				}
				else
				{
					byte[] byteArray = cLevel.userWithBestScore.byteArray;
					Pixmap pixmap = new Pixmap(byteArray, 0, byteArray.length);
					TextureRegion region = ImageHelper.toPotTextureRegion(pixmap);
					friendImage.region.setRegion(region);
					cLevel.userWithBestScore.avatar = region;
				}
			}
			else
			{
				friendImage.region.setRegion(cLevel.userWithBestScore.avatar);
			}
		}
		else
		{
			friendImage.region.setRegion(AssetsGame.getInstance().openfeintRegion);
			((Label)friendScoreLabel).setText( "");
		}
		invalidated = false;
	}

	private void refreshScores(final int level, final Label scoreLabel, final Label friendScoreLabel, final Image friendImage)
	{
		
		new Thread( new Runnable()
		{
			@Override
			public void run()
			{
				CurrentUser currentUser = OpenFeint.getCurrentUser();
				
				if ( currentUser == null )
				{
					System.out.println("OpenFeintHelper.getLevelScore(): OpenFeint not enabled/available.");
					return;
				}
				
				if ( OpenFeintInternal.getInstance() == null )
				{
					System.out.println("OpenFeintHelper.getLevelScore(): OpenFeint not enabled/available.");
					return;
				}
				
				
				
				for (int i = 1; i <= LEVEL_LEADERBOARD.length; i++)
				{
					if ( levels[i-1] == null )
					{
						levels[i-1] = new CLevel(new Leaderboard(LEVEL_LEADERBOARD[i-1]), i);
					}
					final CLevel cLevel = levels[i-1];
					
					getPersonalBestScore(currentUser, cLevel);
					
					getFriendsBestScore(cLevel);
				}
			}
		}).start();
		
	}

	private void getFriendsBestScore(final CLevel cLevel)
	{
		cLevel.leaderboard.getFriendScores(new GetScoresCB()
		{
			@Override
			public void onSuccess(List<Score> scores)
			{
				long tmpScore = 0;
				User tmpUser = null;
				for (Score score : scores)
				{
					if ( score.score > tmpScore )
					{
						tmpScore = score.score;
						tmpUser = score.user;
					}
				}
				if ( tmpUser == null )
				{
					return;
				}
				
				final CUser friendWithBestScore = getUser(tmpUser);
				
				cLevel.putScore(tmpScore, friendWithBestScore );
				
				loadImagesForUser(friendWithBestScore);
				invalidated = true;
			}
			
		});
	}
	
	private void loadImagesForUser( final CUser friendWithBestScore)
	{
		if ( friendWithBestScore.byteArray == null )
		{
//				((Label)friendScoreLabel).setText( LevelSelectorScreen.FRIEND_SCORE + friendsLevelScores[loopLevel-1] );
			friendWithBestScore.user.downloadProfilePicture(new DownloadProfilePictureCB()
			{
				
				@Override
				public void onSuccess(Bitmap iconBitmap)
				{
					ByteArrayOutputStream stream = new ByteArrayOutputStream();
					iconBitmap.compress(CompressFormat.PNG, 100, stream);
					
					friendWithBestScore.byteArray = stream.toByteArray();
					
					OpenFeintHelper.this.invalidated = true;
				}
				
			});
		}
	}
	
	private void getPersonalBestScore(CurrentUser currentUser,
			final CLevel cLevel)
	{
		cLevel.leaderboard.getUserScore(currentUser, new Leaderboard.GetUserScoreCB()
		{
			
			@Override
			public void onSuccess(Score score)
			{
				if ( score != null )
				{
					cLevel.personalBestScore = score.score;
					cLevel.putScore(score.score, getUser(score.user));
				}
				invalidated = true;
			}
		});
	}
	
	public CUser getUser(User user)
	{
		if ( !users.containsKey( user.userID() ) )
		{
			users.put(user.userID(), new CUser(user));
		}
		
		return users.get(user.userID());
	}
	
	public class CLevel
	{
		protected int id;
		public Leaderboard leaderboard;
		public long personalBestScore;
		public long bestScoreFromSelfAndFriends;
		public long bestScoreGlobally;
		public CUser userWithBestScore;
		
		public CLevel(Leaderboard leaderboard2, int pId)
		{
			leaderboard = leaderboard2;
			this.id = pId;
		}

		public void putScore(long tmpScore, CUser friendWithBestScore)
		{
			if ( tmpScore > bestScoreFromSelfAndFriends )
			{
				bestScoreFromSelfAndFriends = tmpScore;
				userWithBestScore = friendWithBestScore;
			}
		}
	}
	
	public class CUser
	{
		public User user;
		public TextureRegion avatar;
		public byte[] byteArray;
		
		public CUser(User user2)
		{
			user = user2;
		}
		
	}

	public void initialize()
	{
//		if ( OpenFeint.getCurrentUser() )
		
		OpenFeintSettings settings = new OpenFeintSettings(CentrifugeAndroidCommon.GAME_NAME, gameKey, gameSecret, gameID);
//		boolean userLoggedIn = OpenFeint.isUserLoggedIn();
//		System.out.println("OpenFeint.isUserLoggedIn: "+ userLoggedIn);
//		if ( userLoggedIn )
//		{
//			OpenFeint.initialize(activity, settings, new OpenFeintDelegate() {});
//		}
//		else
//		{
//		}
		OpenFeint.initialize(activity, settings, null );
//				new OpenFeintDelegate() 
//		{
//			@Override
//			public void userLoggedOut(User user)
//			{
//				super.userLoggedOut(user);
//				System.out.println("OpenFeintHelper.userLoggedOut");
//			}
//			
//			@Override
//			public void userLoggedIn(CurrentUser user)
//			{
//				super.userLoggedIn(user);
//				System.out.println("OpenFeintHelper.userLoggedIn");
//			}
//			
//			@Override
//			public void onDashboardDisappear()
//			{
//				super.onDashboardDisappear();
//				System.out.println("OpenFeintHelper.dashboardDisappear");
//			}
//			
//			@Override
//			public boolean showCustomApprovalFlow(Context ctx)
//			{
//				
//				Builder builder = new AlertDialog.Builder(ctx);
//				builder.setTitle("Would you like to use OpenFeint?");
//				builder.setMessage("OpenFeint tracks your scores and achievements and lets you " +
//						"compete with your frenemies. If you are seeing this message a lot, " +
//						"enable account retreival in feint settings.");
//				builder.setPositiveButton("Log in", new OnClickListener()
//				{
//					
//					@Override
//					public void onClick(DialogInterface dialog, int which)
//					{
//						OpenFeint.userApprovedFeint();
//					}
//				});
//				builder.setNegativeButton("I don't like fun", new OnClickListener()
//				{
//					
//					@Override
//					public void onClick(DialogInterface dialog, int which)
//					{
//						OpenFeint.userDeclinedFeint();
//					}
//				});
//				builder.show();
//				return true;
////				return super.showCustomApprovalFlow(ctx);
//			}
//			
//			
//		});
//		OpenFeint.initializeWithoutLoggingIn(activity, settings, new OpenFeintDelegate() {});

	}

	public void postBreakerBreaker()
	{
		new Achievement(BREAKER_BREAKER).unlock(null);
	}

	public void postAppreciationAward()
	{
		new Achievement(APPRECIATION_AWARD).unlock(null);
	}

	public void post720()
	{
		if ( !posted720 )
		{
			posted720 = true;
			new Achievement(ACHIEVEMENT_720).unlock(null);
		}
	}

	public void postOutOfSight()
	{
		new Achievement(OUT_OF_SIGHT).unlock(null);
	}
}
