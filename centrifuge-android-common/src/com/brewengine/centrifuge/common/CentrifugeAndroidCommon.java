package com.brewengine.centrifuge.common;

import java.util.HashMap;
import java.util.Map;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.view.KeyEvent;
import android.view.WindowManager;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.actors.Label;
import com.brewengine.centrifuge.ActivityCallback;
import com.brewengine.centrifuge.CentrifugeGame;
import com.flurry.android.FlurryAgent;
import com.openfeint.api.ui.Dashboard;

public abstract class CentrifugeAndroidCommon extends AndroidApplication implements ActivityCallback {
	
	public static final String GAME_NAME = "Centrifuge";
	public static final String PACKAGE_FULL = "com.brewengine.centrifuge";
	public static final String PACKAGE_LITE = PACKAGE_FULL + ".lite";
	
	private static final String TRACE_NAME = GAME_NAME.toLowerCase();
	
	private static final String BUILD_BRAND_KEY        = "build brand";
	private static final String BUILD_DEVICE_KEY       = "build device";
	private static final String BUILD_ID_KEY           = "build id";
	private static final String BUILD_MODEL_KEY        = "build model";
	private static final String BUILD_MANUFACTURER_KEY = "build manufacturer";
	
	private OpenFeintHelper openFeintHelper;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ApplicationListener listener = new CentrifugeGame(this);
        
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useGL20 = false;
        config.useAccelerometer = true;
        config.useCompass = false;
        
        initialize(listener, config);
        Gdx.input.setCatchBackKey(true);
        
        this.openFeintHelper = new OpenFeintHelper(this);
        openFeintHelper.initialize();
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
    public abstract boolean isLite();
    
    @Override
    public void askToPurchaseFullVersion() {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				AlertDialog dialog = new AlertDialog.Builder(CentrifugeAndroidCommon.this).create();
				
				dialog.setTitle("Full Version Required");
				dialog.setMessage("The level selected is only available in the full version of " + GAME_NAME + ". Proceed to the Android Market?");
				
				dialog.setButton("Cancel", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
					
				});
				
				dialog.setButton2("OK", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData( Uri.parse("market://details?id=" + PACKAGE_FULL) );
						startActivity(intent);
					}
					
				});
				
				dialog.show();
			}
		});
	}

	@Override
	public void executeLink(final String url)
	{
		
		if ( url == "http://www.brewengine.com" )
		{
			postAppreciationAward();
		}
		
		runOnUiThread(new Runnable()
		{
			
			@Override
			public void run()
			{
				AlertDialog dialog = new AlertDialog.Builder(CentrifugeAndroidCommon.this).create();
				
				String http = "http://";
				String showURL = url;
				if ( url.startsWith(http ) )
				{
					showURL = url.substring(http.length() );
				}
				
				dialog.setTitle("Credits");
				dialog.setMessage("This will launch "+ showURL + " in your browser.");
				
				dialog.setButton("Keep playing", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				dialog.setButton2("Go", new DialogInterface.OnClickListener()
				{
					
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
						startActivity(browserIntent);
					}
				});
				
				dialog.show();
			}
		});
	}
	
	@Override
	public Map<String, String> buildDetails() {
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put(BUILD_BRAND_KEY,        Build.BRAND);
		map.put(BUILD_DEVICE_KEY,       Build.DEVICE);
		map.put(BUILD_ID_KEY,           Build.ID);
		map.put(BUILD_MANUFACTURER_KEY, Build.MANUFACTURER);
		map.put(BUILD_MODEL_KEY,        Build.MODEL);
		
		return map;
	}
	
	@Override
	public void flurryEvent(String eventId, HashMap<String, String> parameters) {
		Gdx.app.log("CentrifugeDesktopGame.flurryEvent", "event id = " + eventId + ", parameters = " + parameters);
		FlurryAgent.onEvent(eventId, parameters);
	}
	
	/*
	 * OpenFeint methods
	 */
	
	@Override
	public boolean isInvalid()
	{
		return openFeintHelper.invalidated;
	}
	
	@Override
	public void showScores()
	{
		Dashboard.open();
	}
	
	@Override
	public void getLevelScores(int level, final Label scoreLabel, Label friendScoreLabel, Image friendImage)
	{
		openFeintHelper.getLevelScore(level, scoreLabel, friendScoreLabel, friendImage);
	}
	
	@Override
	public void postScore(int level, int score)
	{
		openFeintHelper.postScore(level, score);
	}
	
	@Override
	public void postStreak(int score)
	{
		openFeintHelper.postStreak(score);
	}
	
	@Override
	public void postNoHitter()
	{
		openFeintHelper.postNoHitter();
	}
	
	private void postAppreciationAward()
	{
		openFeintHelper.postAppreciationAward();
	}

	@Override
	public void postBreakerBreaker()
	{
		openFeintHelper.postBreakerBreaker();
	}
	
	@Override
	public void post720()
	{
		openFeintHelper.post720();
	}
	
	@Override
	public void postOutOfSight()
	{
		openFeintHelper.postOutOfSight();
	}
	
	/*
	 * Trace methods
	 */
	
	// TODO comment this method prior to release
//	@Override
//	public boolean onKeyDown(int keyCode, KeyEvent event) {
//		if (keyCode == KeyEvent.KEYCODE_MENU) {
//			startTrace();
//		} else if (keyCode == KeyEvent.KEYCODE_SEARCH) {
//			stopTrace();
//		}
//		
//		return super.onKeyDown(keyCode, event);
//	}

	private void startTrace() {
		Gdx.app.log("CentrifugeAndroidCommon.startTrace", "Starting trace: " + TRACE_NAME);
		Debug.startMethodTracing(TRACE_NAME);
	}
	
	private void stopTrace() {
		Gdx.app.log("CentrifugeAndroidCommon.startTrace", "Stopping trace: " + TRACE_NAME);
		Debug.stopMethodTracing();
	}
	
}
