package com.brewengine.centrifuge;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.actors.Label;
import com.brewengine.centrifuge.screens.LevelSelectorScreen;

public class CentrifugeDesktopGame {
	
	public enum Device {
		LevelEditor,
		MotorolaDroid,
		MotorolaDroidX,
		MotorolaTriumph,
		LGOptimusV,
		SamsungGalaxyTab7, // 7 inch screen
		SamsungGalaxyTab10, // 10.1 inch screen
	}
	
	private static final String BUILD_BRAND_KEY        = "build brand";
	private static final String BUILD_DEVICE_KEY       = "build device";
	private static final String BUILD_ID_KEY           = "build id";
	private static final String BUILD_MODEL_KEY        = "build model";
	private static final String BUILD_MANUFACTURER_KEY = "build manufacturer";
	
	public static void main(String[] args) {
		ApplicationListener listener = new CentrifugeGame(new CentrifugeDesktopGame().new DummyCallback());
		
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Centrifuge";
		config.useGL20 = false;
		
		setResolution(config, Device.LevelEditor);
		
//		DisplayMode displayMode = LwjglApplicationConfiguration.getDesktopDisplayMode();
//		config.setFromDisplayMode(displayMode);
		
		new LwjglApplication(listener, config);
	}
	
	public static void setResolution(LwjglApplicationConfiguration config, Device device) {
		// since we are in landscape mode, width and height are swapped from that of standard portrait specs
		switch (device) {
		case LevelEditor: // we use the widest known aspect ratio to insure we don't make any obstacles off screen for other devices
		case MotorolaDroid:
		case MotorolaDroidX:
			config.width  = 854;
			config.height = 480;
			break;
		case MotorolaTriumph:
			config.width  = 800;
			config.height = 480;
			break;
		case LGOptimusV:
			config.width  = 480;
			config.height = 320;
			break;
		case SamsungGalaxyTab7:
			config.width = 1024;
			config.height = 600;
			break;
		case SamsungGalaxyTab10:
			config.width = 1280;
			config.height = 800;
			break;
		}
	}

	public class DummyCallback implements ActivityCallback {
		
		@Override
		public void postScore(int level, int score) {
			Gdx.app.log("CentrifugeDesktopGame.postScore", "Android should be posting the score right about now.");
		}

		@Override
		public void showScores()
		{
			Gdx.app.log("CentrifugeDesktopGame.postScore", "Android should be showing the scores.");
		}

		@Override
		public void getLevelScores(int level, Label scoreLabel, Label friendScoreLabel, Image friendImage) {
			Gdx.app.log("CentrifugeDesktopGame.postScore", "Android should be showing the scores per level.");
			scoreLabel.setText( LevelSelectorScreen.BEST_SCORE + "n/a" );
			friendScoreLabel.setText( LevelSelectorScreen.BEST_SCORE + "n/a" );
		}

		@Override
		public boolean isInvalid()
		{
			return false;
		}

		@Override
		public boolean isLite() {
			return false;
		}
		
		@Override
		public void executeLink(String string) {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.executeLink", "Ignoring action on desktop. link = " + string);
		}

		@Override
		public void askToPurchaseFullVersion() {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.askToPurchaseFullVersion", "Ignoring action on desktop.");
		}

		@Override
		public void postStreak(int streak) {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.postStreak", "Ignoring OpenFeint action on desktop. streak = " + streak);
		}

		@Override
		public void postNoHitter() {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.postNoHitter", "Ignoring OpenFeint action on desktop.");
		}

		@Override
		public void postBreakerBreaker() {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.postBreakerBreaker", "Ignoring OpenFeint action on desktop.");
		}

		public void flurryEvent(String eventId, HashMap<String, String> parameters) {
			Gdx.app.log("CentrifugeDesktopGame.flurryEvent", "event id = " + eventId + ", parameters = " + parameters);
		}

		@Override
		public void post720() {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.post720", "Ignoring OpenFeint action on desktop.");
		}

		@Override
		public void postOutOfSight() {
			Gdx.app.log("CentrifugeDesktopGame.DummyCallback.postOutOfSight", "Ignoring OpenFeint action on desktop.");
		}

		@Override
		public Map<String, String> buildDetails() {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put(BUILD_DEVICE_KEY, "Desktop");
			return map;
		}

	}
}
