package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

/**
 * Provides helper methods for math operations.
 * 
 * All angles are considered to be in radians (unless otherwise noted) relative
 * to 0 at 3-o'clock with counter-clockwise rotation being positive.
 */
public class MathHelper
{
	public static final int COUNTER_CLOCKWISE = 1;
	public static final int CLOCKWISE = -1;
	public static final int OPPOSITE = 0;

	public static final float HALF_PI = (float) (Math.PI / 2);
	public static final float PI = (float) Math.PI;
	public static final float TWO_PI = (float) (2 * Math.PI);

	/*
	 * Directional constants with 0 radians at 3-o'clock.
	 */
	public static final float NORTH = HALF_PI;
	public static final float EAST = 0f;
	public static final float SOUTH = -HALF_PI;
	public static final float WEST = PI;
	
	/*
	 * Directional constants with 0 degrees at 3-o'clock.
	 */
	public static final float NORTH_DEGREES = 90f;
	public static final float EAST_DEGREES  = 0f;
	public static final float SOUTH_DEGREES = -90f;
	public static final float WEST_DEGREES  = 180f;
	
	/**
	 * Determines the distance between two points.
	 * 
	 * Use {@link distanceSquared} whenever possible, as it is faster.
	 * 
	 * @param pPointA
	 * @param pPointB
	 * @return
	 */
	public static float distance(Vector2 pPointA, Vector2 pPointB) {
		return (float) Math.sqrt(distanceSquared(pPointA, pPointB));
	}
	
	/**
	 * Determines the distance between two points.
	 * 
	 * Use {@link distanceSquared} whenever possible, as it is faster.
	 * 
	 * @param pX1
	 * @param pY1
	 * @param pX2
	 * @param pY2
	 * @return
	 */
	public static float distance(float pX1, float pY1, float pX2, float pY2) {
		return (float) Math.sqrt(distanceSquared(pX1, pY1, pX2, pY2));
	}
	
	/**
	 * Determines the squared distance between two points.
	 * 
	 * @param pPointA
	 * @param pPointB
	 * @return
	 */
	public static float distanceSquared(Vector2 pPointA, Vector2 pPointB) {
		return distanceSquared(pPointA.x, pPointA.y, pPointB.x, pPointB.y);
	}
	
	/**
	 * Determines the squared distance between two points.
	 * 
	 * @param pX1
	 * @param pY1
	 * @param pX2
	 * @param pY2
	 * @return
	 */
	public static float distanceSquared(float pX1, float pY1, float pX2, float pY2) {
		float diffX = pX2 - pX1;
		float diffY = pY2 - pY1;
		return diffX * diffX + diffY * diffY;
	}
	
	/**
	 * Float-based arc tangent.
	 * 
	 * @param pX
	 * @param pY
	 * @return
	 */
	public static float atan2f(float pX, float pY) {
		return (float) Math.atan2(pY, pX);
	}
	
	/**
	 * Rotates point about the origin.
	 * 
	 * {@link http://en.wikipedia.org/wiki/Rotation_%28mathematics%29}
	 * 
	 * @param pPoint Point that will be rotated.
	 * @param pRotation Positive rotation results in counter-clockwise rotation.
	 */
	public static void rotatePoint(Vector2 pPoint, float pRotation) {
		if (pRotation == 0f) {
			return;
		}
		
		float x = MathUtils.cos(pRotation) * pPoint.x - MathUtils.sin(pRotation) * pPoint.y;
		float y = MathUtils.sin(pRotation) * pPoint.x + MathUtils.cos(pRotation) * pPoint.y;
		pPoint.set(x, y);
	}
	
	/**
	 * Rotates point about the origin.
	 * 
	 * {@link http://en.wikipedia.org/wiki/Rotation_%28mathematics%29}
	 * 
	 * @param pPoint Point that will be rotated.
	 * @param pRotationInDegrees Positive rotation results in counter-clockwise rotation.
	 */
	public static void rotatePointDegrees(Vector2 pPoint, float pRotationInDegrees) {
		if (pRotationInDegrees == 0f) {
			return;
		}
		
		float x = MathUtils.cosDeg(pRotationInDegrees) * pPoint.x - MathUtils.sinDeg(pRotationInDegrees) * pPoint.y;
		float y = MathUtils.sinDeg(pRotationInDegrees) * pPoint.x + MathUtils.cosDeg(pRotationInDegrees) * pPoint.y;
		pPoint.set(x, y);
	}
	
	/**
	 * Normalizes angle to be between -pi (exclusive) and pi (inclusive).
	 * 
	 * @param pAngle Reference angle.
	 * @return Angle between -pi (exclusive) and pi (inclusive).
	 */
	public static float normalizeAngle(float pAngle) {
		return normalizeAngle(pAngle, -PI, PI, TWO_PI);
	}
	
	/**
	 * Normalizes angle to be between -180 degrees (exclusive) and 180 degrees
	 * (inclusive).
	 * 
	 * @param pAngleInDegrees Reference angle in degrees.
	 * @return Angle between -180 (exclusive) and 180 (inclusive) in degrees.
	 */
	public static float normalizeAngleDegrees(float pAngleInDegrees) {
		return normalizeAngle(pAngleInDegrees, -180, 180, 360);
	}
	
	/**
	 * Normalizes angle to be between min (exclusive) and max (inclusive) angles.
	 * 
	 * {@link http://stackoverflow.com/questions/2320986/easy-way-to-keeping-angles-between-179-and-180-degrees}
	 * 
	 * @param pAngle Reference angle.
	 * @param pMin
	 * @param pMax
	 * @param pIncrement
	 * @return Normalized angle between pMin (exclusive) and pMax (inclusive).
	 */
	public static float normalizeAngle(float pAngle, float pMin, float pMax, float pIncrement) {
		while (pAngle <= pMin) pAngle += pIncrement;
		while (pAngle > pMax) pAngle -= pIncrement;
		return pAngle;
	}
	
	/**
	 * Determines the angle between two angles.
	 * 
	 * @param pAngle1 First angle.
	 * @param pAngle2 Second angle.
	 * @return
	 */
	public static float angleBetweenAngles(float pAngle1, float pAngle2) {
		float difference = MathHelper.normalizeAngle( differenceOfAngles(pAngle1, pAngle2) );
		return Math.abs(difference);
	}
	
	/**
	 * Determines the angle between two angles (in degrees).
	 * 
	 * @param pAngle1InDegrees First angle in degrees.
	 * @param pAngle2InDegrees Second angle in degrees.
	 * @return
	 */
	public static float angleBetweenAnglesDegrees(float pAngle1InDegrees, float pAngle2InDegrees) {
		float difference = differenceOfAnglesDegrees(pAngle1InDegrees, pAngle2InDegrees);
		return Math.abs(difference);
	}
	
	/**
	 * Determines the difference between two angles normalizing the result to
	 * be between -pi (exclusive) and pi (inclusive).
	 * 
	 * @param pAngle1
	 * @param pAngle2
	 * @return Difference of pAngle1 and pAngle2 normalized to be between -pi (exclusive) and pi (inclusive).
	 */
	public static float differenceOfAngles(float pAngle1, float pAngle2) {
		float difference = pAngle1 - pAngle2;
		return normalizeAngle(difference);
	}
	
	/**
	 * Determines the difference between two angles normalizing the result to
	 * be between -180 degrees (exclusive) and 180 degrees (inclusive).
	 * 
	 * @param pAngle1InDegrees
	 * @param pAngle2InDegrees
	 * @return Difference of pAngle1 and pAngle2 normalized to be between -180 (exclusive) and 180 (inclusive).
	 */
	public static float differenceOfAnglesDegrees(float pAngle1InDegrees, float pAngle2InDegrees) {
		float difference = pAngle1InDegrees - pAngle2InDegrees;
		return normalizeAngleDegrees(difference);
	}
	
	/**
	 * Determines if shortest distance from pAngle1 to pAngle2 is a clockwise or
	 * counter-clockwise rotation, or angles are directly opposite when
	 * comparison is done according to a cartesian coordinate frame (where
	 * counter-clockwise rotation produces an increase in angle).
	 * 
	 * This function adheres to the "right hand rule" with positive being out of
	 * the screen and negative being into the screen.
	 * 
	 * {@link http://en.wikipedia.org/wiki/Right-hand_rule}
	 * 
	 * @param pAngle1
	 * @param pAngle2
	 * @return COUNTER_CLOCKWISE or CLOCKWISE travelling from pAngle1 to pAngle2, or OPPOSITE if angles are directly opposing each other.
	 */
	public static int compareAngles(float pAngle1, float pAngle2) {
		float crossProduct = crossProduct(pAngle1, pAngle2);
		
		if (crossProduct < 0) {
			return COUNTER_CLOCKWISE;
		} else if (crossProduct > 0) {
			return CLOCKWISE;
		}
		
		return OPPOSITE;
	}
	
	/**
	 * Determines if shortest distance from pAngle1 to pAngle2 is a clockwise or
	 * counter-clockwise rotation, or angles are directly opposite when
	 * comparison is done according to a cartesian coordinate frame (where
	 * counter-clockwise rotation produces an increase in angle).
	 * 
	 * All angles are in degrees.
	 * 
	 * This function adheres to the "right hand rule" with positive being out of
	 * the screen and negative being into the screen.
	 * 
	 * {@link http://en.wikipedia.org/wiki/Right-hand_rule}
	 * 
	 * @param pAngle1InDegrees Reference angle.
	 * @param pAngle2InDegrees
	 * @return COUNTER_CLOCKWISE or CLOCKWISE travelling from pAngle1 to pAngle2, or OPPOSITE if angles are directly opposing each other.
	 */
	public static int compareAnglesDegrees(float pAngle1InDegrees, float pAngle2InDegrees) {
		float crossProduct = crossProductDegrees(pAngle1InDegrees, pAngle2InDegrees);
		
		if (crossProduct < 0) {
			return COUNTER_CLOCKWISE;
		} else if (crossProduct > 0) {
			return CLOCKWISE;
		}
		
		return OPPOSITE;
	}
	
	/**
	 * Magnitudeless cross product of two angles (in radians).
	 * {@link http://en.wikipedia.org/wiki/Cross_product}
	 * 
	 * @param pAngle1
	 * @param pAngle2
	 * @return
	 */
	public static float crossProduct(float pAngle1, float pAngle2) {
		float difference = differenceOfAngles(pAngle1, pAngle2);
		return (float) MathUtils.sin(difference);
	}
	
	/**
	 * Magnitudeless cross product of two angles (in degrees).
	 * {@link http://en.wikipedia.org/wiki/Cross_product}
	 * 
	 * @param pAngle1InDegrees
	 * @param pAngle2InDegrees
	 * @return
	 */
	public static float crossProductDegrees(float pAngle1InDegrees, float pAngle2InDegrees) {
		float difference = differenceOfAnglesDegrees(pAngle1InDegrees, pAngle2InDegrees);
		return (float) MathUtils.sinDeg(difference);
	}
	
	/**
	 * Returns a Cartesian vector with specified magnitude and angle.
	 * 
	 * @param pMagnitude
	 * @param pAngleInRadians Angle relative to 3 o'clock with counter-clockwise rotation being positive.
	 * @return
	 */
	public static Vector2 vector(float pMagnitude, float pAngleInRadians) {
		Vector2 unitVector = unitVector(pAngleInRadians);
		return unitVector.mul(pMagnitude);
	}
	
	/**
	 * Returns a Cartesian vector with specified magnitude and angle.
	 * 
	 * @param pMagnitude
	 * @param pAngleInRadians Angle in degrees relative to 3 o'clock with counter-clockwise rotation being positive.
	 * @return
	 */
	public static void vectorFromDegrees(float pMagnitude, float pAngleInDegrees, Vector2 vector) {
		unitVectorFromDegreesNew(pAngleInDegrees, vector);
		vector.mul(pMagnitude);
	}
	
	/**
	 * Returns the unit vector formed by the specified angle.
	 * 
	 * @param pAngleInRadians Angle relative to 3 o'clock with counter-clockwise rotation being positive.
	 * @return
	 */
	public static Vector2 unitVector(float pAngleInRadians) {
		return new Vector2(MathUtils.cos(pAngleInRadians), MathUtils.sin(pAngleInRadians));
	}
	
	/**
	 * Returns the unit vector formed by the specified angle.
	 * 
	 * @param pAngleInDegrees Angle in degrees relative to 3 o'clock with counter-clockwise rotation being positive.
	 * @return
	 */
	public static void unitVectorFromDegreesNew(float pAngleInDegrees, Vector2 vector) {
		vector.set(MathUtils.cosDeg(pAngleInDegrees), MathUtils.sinDeg(pAngleInDegrees));
	}
	
	/**
	 * Rotation from point 1 to point 2.
	 * 
	 * @param targetX
	 * @param targetY
	 * @return Rotation in degrees relative to 3-o'clock, increasing counter-clockwise.
	 */
	public static float rotationFromPointToPoint(Vector2 pPoint1, Vector2 pPoint2) {
		float diffX = pPoint2.x - pPoint1.x;
		float diffY = pPoint2.y - pPoint1.y;
		float rotationToPoint2 = MathUtils.atan2(diffY, diffX) * MathUtils.radiansToDegrees;
		
		return rotationToPoint2;
	}
	
	public static Vector2 offsetWithRotation(Vector2 pOffset, float pRotation) {
		return new Vector2(pOffset).rotate(pRotation);
	}
	
	public static double discriminant(float pA, float pB, float pC) {
		// b ^ 2 - 4 * a * c
		return Math.pow(pB, 2) - 4 * pA * pC;
	}
}
