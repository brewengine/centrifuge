package com.brewengine.centrifuge.helpers.controls;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

public class ArrowKeys extends InputAdapter implements Controls {
	
	private float direction;
	private float rotation;
	private float duration;

	@Override
	public float getRotation() {
		return rotation;
	}
	
	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
		case Keys.DPAD_LEFT:
			direction = 1f;
			duration = 0f;
			return true;

		case Keys.DPAD_RIGHT:
			direction = -1f;
			duration = 0f;
			return true;
		}
		
		return false;
	}
	
	public boolean keyUp(int keycode) {
		if (keycode == Keys.DPAD_LEFT || keycode == Keys.DPAD_RIGHT) {
			direction = 0f;
			return true;
		}
		return false;
	}

	@Override
	public void update(float delta) {
		duration += delta * 25f;
		rotation += delta * duration * direction * 10f;
	}

}
