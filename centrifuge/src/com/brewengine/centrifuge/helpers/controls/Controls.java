package com.brewengine.centrifuge.helpers.controls;

public interface Controls {
	public void update(float delta);
	public float getRotation();
}
