package com.brewengine.centrifuge.helpers.controls;

import com.badlogic.gdx.math.Vector2;
import com.brewengine.centrifuge.helpers.SmoothAccelerometer;

public class Accelerometer implements Controls {
	
	private Vector2 rotation = new Vector2();
	
	@Override
	public float getRotation() {
		rotation.set(SmoothAccelerometer.getAccelerometerX(), SmoothAccelerometer.getAccelerometerY());
		return rotation.angle();
	}

	@Override
	public void update(float delta) {
		
	}
	
}
