package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer10;
import com.badlogic.gdx.math.Vector2;

public class ImmediateModeRendererHelper {
	
	public static ImmediateModeRenderer10 renderer = new ImmediateModeRenderer10();
	
	public static void drawLine(Vector2...points) {
		drawLine(Color.WHITE, points);
	}
	
	public static void drawLine(Color color, Vector2...points) {
		drawLine(color, GL10.GL_LINES, points);
	}
	
	public static void drawLine(Color color, int type, Vector2...points) {
		float[] p = new float[points.length * 2];
		for (int i = 0; i < points.length; i++) {
			Vector2 point = points[i];
			p[i] = point.x;
			p[i + 1] = point.y;
		}
		drawLine(color, type, p);
	}
	
	public static void drawLine(Color color, float...points) {
		drawLine(color, GL10.GL_LINES, points);
	}
	
	/**
	 * Draws a line or a set of lines from the provided series of points.
	 * 
	 * @param color
	 * @param type
	 * @param points Primitive array of floats that alternate x and y, e.g. {x1, y1, x2, y2, ...}.
	 */
	public static void drawLine(Color color, int type, float...points) {
		if (points.length < 4) {
			// a line must contain at least two pairs of floats (4 floats total)
			return;
		}
		
		renderer.begin(type);
		for (int i = 1; i < points.length; i += 2) {
			float a = points[i - 1];
			float b = points[i];
			renderer.color(color.r, color.g, color.b, color.a);
            renderer.vertex(a, b, 0);
		}
		renderer.end();
	}
	
}
