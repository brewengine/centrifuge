package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;

public class ScaleHelper {
	
	public static float scaleToFit(Table pTable, float pWidth, float pHeight) {
		float scaleX = pWidth  / pTable.getPrefWidth();
		float scaleY = pHeight / pTable.getPrefHeight();
		float scale = 1f;
		
		if (pTable.getPrefWidth() < pWidth && pTable.getPrefHeight() < pHeight) {
			// both width and height need to be scaled up, scale by the larger scale to fill
			scale = Math.min(scaleX, scaleY);
		} else if (pTable.getPrefWidth() > pWidth || pTable.getPrefHeight() > pHeight) {
			// width or height is too large, using smallest scale to fit
			scale = Math.min(scaleX, scaleY);
		}
		
		pTable.scaleX = scale;
		pTable.scaleY = scale;
		
		return scale;
	}

}
