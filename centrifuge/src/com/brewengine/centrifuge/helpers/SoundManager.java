package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

public class SoundManager {

	public static Music music;

	public static void load() {
		FileHandle file = Gdx.files.internal("music/staff.mp3");
		music = Gdx.audio.newMusic(file);
		music.setLooping(true);
	}
	
}
