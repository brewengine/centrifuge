package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import com.brewengine.centrifuge.models.Grid;

public class GridRenderer {

	public Grid grid;
	public Rectangle bounds;
	public final Color color = new Color(Color.WHITE);
	
	public GridRenderer(Grid pGrid, Rectangle pBounds) {
		grid = pGrid;
		bounds = pBounds;
	}
	
	public void draw() {
		float top = bounds.y + bounds.height;
		float right = bounds.x + bounds.width;
		
		// vertical lines left of origin
		for (float x = grid.origin.x; x >= bounds.x; x -= grid.spacing.x) {
			ImmediateModeRendererHelper.drawLine(color, x, bounds.y, x, top);
		}
		
		// vertical lines right of origin
		for (float x = grid.origin.x; x <= right; x += grid.spacing.x) {
			ImmediateModeRendererHelper.drawLine(color, x, bounds.y, x, top);
		}
		
		// horizontal lines below origin
		for (float y = grid.origin.y; y >= bounds.y; y -= grid.spacing.y) {
			ImmediateModeRendererHelper.drawLine(color, bounds.x, y, right, y);
		}
		
		// horizontal lines above origin
		for (float y = grid.origin.y; y <= top; y += grid.spacing.y) {
			ImmediateModeRendererHelper.drawLine(color, bounds.x, y, right, y);
		}
	}
	
}
