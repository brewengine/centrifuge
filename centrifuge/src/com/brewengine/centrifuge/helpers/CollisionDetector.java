package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.glutils.ImmediateModeRenderer10;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.brewengine.centrifuge.actors.Obstacle;
import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.controllers.GameController;
import com.brewengine.centrifuge.stages.GameStage;

public class CollisionDetector {
	
	public boolean debug;
	private ImmediateModeRenderer10 renderer = new ImmediateModeRenderer10();
	
	private final GameController mGameController;
	private Vector2 p = new Vector2();
	
	public CollisionDetector(GameController pGameController) {
		mGameController = pGameController;
	}

	public void check() {
		GameStage stage = mGameController.stage;
		
		for (Particle particle : stage.rotator.getParticles()) {
			for (Obstacle obstacle : stage.getObstacles()) {
				p.set(particle.x, particle.y);
				
				/*
				 * Actor's x and y are defined as the bottom left corner, so we
				 * add half the width and height to establish x and y as the
				 * center of the particle.
				 */
				p.add(particle.width / 2, particle.height / 2);
				
				stage.rotatorToStageCoordinates(p);
				Circle particleCircle = new Circle(p.x, p.y, particle.width / 2);
				
				if (debug) {
					drawCircle(particleCircle, Color.GREEN);
				}
				
				if (obstacle.type == Obstacle.Type.Standard || obstacle.type == Obstacle.Type.Converted) {
					Circle obstacleCircle = new Circle(obstacle.x + obstacle.width / 2, obstacle.y + obstacle.height / 2, obstacle.width / 2);
					
					if (debug) {
						drawCircle(obstacleCircle, Color.RED);
					}
					
					if (Intersector.overlapCircles(obstacleCircle, particleCircle)) {
//						rotated.color.set(Color.RED);
						mGameController.collision(particle, obstacle);
						continue;
					}
				} else if (obstacle.type == Obstacle.Type.Goal) {
					Rectangle obstacleRectangle = new Rectangle(obstacle.x, obstacle.y, obstacle.width, obstacle.height);
					
					if (debug) {
						drawRectangle(obstacleRectangle, Color.WHITE);
					}
					
					if (Intersector.overlapCircleRectangle(particleCircle, obstacleRectangle)) {
						mGameController.collision(particle, obstacle);
						break;
					}
				}
			}
		}
	}
	
	public interface CollisionDetectorListener {
		public void collision(Particle pParticle, Obstacle pObstacle);
	}
	
	/*
	 * Debugging methods.
	 */
	
	private void drawCircle(Circle pCircle, Color pColor) {
		double points = 10;
		float angle = 0;
		float increment = (float) (2 * Math.PI / points);
		Vector2 tempVector = new Vector2();

		renderer.begin(GL10.GL_LINE_LOOP);
			for (int i = 0; i < points; i++, angle += increment) {
				tempVector.set((float)Math.cos(angle) * pCircle.radius + pCircle.x, (float)Math.sin(angle) * pCircle.radius + pCircle.y);
	            renderer.color(pColor.r, pColor.g, pColor.b, pColor.a);
	            renderer.vertex(tempVector.x, tempVector.y, 0);
			}
		renderer.end();
	}
	
	private void drawRectangle(Rectangle pRectangle, Color pColor) {
		Vector2[] points = {
			new Vector2(pRectangle.x, pRectangle.y),
			new Vector2(pRectangle.x + pRectangle.width, pRectangle.y),
			new Vector2(pRectangle.x + pRectangle.width, pRectangle.y + pRectangle.height),
			new Vector2(pRectangle.x, pRectangle.y + pRectangle.height),
		};
		
		renderer.begin(GL10.GL_LINE_LOOP);
			for (int i = 0; i < points.length; i++) {
				Vector2 point = points[i];
	            renderer.color(pColor.r, pColor.g, pColor.b, pColor.a);
	            renderer.vertex(point.x, point.y, 0);
			}
		renderer.end();
	}
	
}
