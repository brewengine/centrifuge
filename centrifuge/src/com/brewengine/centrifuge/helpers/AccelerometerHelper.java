package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.Gdx;

public class AccelerometerHelper {

	private static float lastX;
	private static float lastY;
	private static float lastZ;
	
	private static int UPS; // updates per second
	private static int count;
	private static long start;
	
	private static int totalUPS;
	private static int totalUPSCount;
	
	public static void update() {
		long time = System.nanoTime();
		if (time - start >= 1000000000) {
			UPS = count;
			
			totalUPS += UPS;
			totalUPSCount++;
			
			count = 0;
			start = time;
		}
		
		float x = Gdx.input.getAccelerometerX();
		float y = Gdx.input.getAccelerometerY();
		float z = Gdx.input.getAccelerometerZ();
		
		if (x != lastX || y != lastY || z != lastZ) {
			lastX = x;
			lastY = y;
			lastZ = z;
			
			count++;
		}
	}
	
	/**
	 * Returns the current accelerometer resolution.
	 * 
	 * @return Accelerometer changes per second.
	 */
	public static int getResolution() {
		return UPS;
	}
	
	public static float getAverageResolution() {
		if (totalUPSCount == 0) {
			return 0;
		}
		
		return totalUPS / (float) totalUPSCount;
	}
}
