package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.brewengine.centrifuge.AssetsGame;

public class TiltIndicator {

	public static final float Z_THRESHOLD = 7f;

	private static final float FRAME_DURATION = 0.25f;
	private static final float PADDING = 3;
	
	public Group group;
	
	private Image correct;
	private Image incorrect;
	private Image x;
	
	private float time;
	private Animation animationCorrect;
	private Animation animationIncorrect;
	
	public TiltIndicator() {
		animationCorrect   = new Animation(FRAME_DURATION, AssetsGame.getInstance().tiltCorrect);
		animationIncorrect = new Animation(FRAME_DURATION, AssetsGame.getInstance().tiltIncorrect);
		
		correct = new Image("correct tilt", animationCorrect.getKeyFrame(0, true));
		incorrect = new Image("incorrect tilt", animationIncorrect.getKeyFrame(0, true));
		x = new Image("x", AssetsGame.getInstance().xRegion);
		
		correct.color.a = 0.4f;
		incorrect.color.a = correct.color.a;
		x.color.a = 0.6f;
		
		incorrect.x = correct.width + PADDING;
		x.x = incorrect.x;
		x.y = incorrect.y;
		
		group = new Group("tilt indicator");
		group.visible = false;
		group.width = correct.width + PADDING + incorrect.width;
		group.height = Math.max(correct.height, incorrect.height);
		
		group.addActor(correct);
		group.addActor(incorrect);
		group.addActor(x);
	}
	
	public void update(float delta) {
		float z = Gdx.input.getAccelerometerZ();
		
		if (Math.abs(z) > Z_THRESHOLD) {
			show();
		} else {
			hide();
		}
		
		if (group.visible) {
			time += delta;
			
			// animations
			correct.region.setRegion(animationCorrect.getKeyFrame(time, true));
			incorrect.region.setRegion(animationIncorrect.getKeyFrame(time, true));
			x.visible = (int)(time / FRAME_DURATION) % 2 == 1;
		}
	}
	
	public void show() {
		if (group.visible) {
			return;
		}
		
		time = 0;
		group.visible = true;
	}
	
	public void hide() {
		group.visible = false;
	}
	
}
