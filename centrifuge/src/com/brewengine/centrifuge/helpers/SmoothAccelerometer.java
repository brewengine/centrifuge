package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.Gdx;

/**
 * Smooths accelerometer input using a basic low-pass filter.
 * {@link http://stackoverflow.com/questions/2272527/how-do-you-use-a-moving-average-to-filter-out-accelerometer-values-in-iphone-os}
 */
public class SmoothAccelerometer {

	public static float filteringFactor = 0.9f;
	
	private static float x;
	private static float y;
	
	public static float getAccelerometerX() {
		x = filteringFactor * x + (1.0f - filteringFactor) * Gdx.input.getAccelerometerX();
		return x;
	}
	
	public static float getAccelerometerY() {
		y = filteringFactor * y + (1.0f - filteringFactor) * Gdx.input.getAccelerometerY();
		return y;
	}
	
}
