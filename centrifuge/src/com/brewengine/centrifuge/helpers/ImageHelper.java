package com.brewengine.centrifuge.helpers;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;

public class ImageHelper {
	
	public static TextureRegion toPotTextureRegion(Pixmap nonPotPixmap) {
        int width = MathUtils.nextPowerOfTwo(nonPotPixmap.getWidth());
        int height = MathUtils.nextPowerOfTwo(nonPotPixmap.getHeight());
        
        Pixmap potPixmap = new Pixmap(width, height, nonPotPixmap.getFormat());
        potPixmap.drawPixmap(nonPotPixmap, 0, 0, 0, 0, nonPotPixmap.getWidth(), nonPotPixmap.getHeight());
        
        TextureRegion region = new TextureRegion(new Texture(potPixmap), 0, 0, nonPotPixmap.getWidth(), nonPotPixmap.getHeight());
        
        nonPotPixmap.dispose();
        potPixmap.dispose();
        
        return region;
	}

}
