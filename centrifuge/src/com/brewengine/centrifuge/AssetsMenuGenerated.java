package com.brewengine.centrifuge;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.Gdx;

/* AUTO GENERATED FILE */

public abstract class AssetsMenuGenerated {

	public static final String BORDER_BOTTOM_CENTER = "borderBottomCenter";
	public static final String BORDER_BOTTOM_LEFT = "borderBottomLeft";
	public static final String BORDER_BOTTOM_RIGHT = "borderBottomRight";
	public static final String BORDER_MIDDLE_CENTER = "borderMiddleCenter";
	public static final String BORDER_MIDDLE_LEFT = "borderMiddleLeft";
	public static final String BORDER_MIDDLE_RIGHT = "borderMiddleRight";
	public static final String BORDER_TOP_CENTER = "borderTopCenter";
	public static final String BORDER_TOP_LEFT = "borderTopLeft";
	public static final String BORDER_TOP_RIGHT = "borderTopRight";
	public static final String BREWENGINE = "brewengine";
	public static final String LIBGDX = "libgdx";
	public static final String LOGO = "logo";
	public static final String MENU_FONT = "menuFont";

	public TextureAtlas textures;
	public TextureRegion borderBottomCenterRegion;
	public TextureRegion borderBottomLeftRegion;
	public TextureRegion borderBottomRightRegion;
	public TextureRegion borderMiddleCenterRegion;
	public TextureRegion borderMiddleLeftRegion;
	public TextureRegion borderMiddleRightRegion;
	public TextureRegion borderTopCenterRegion;
	public TextureRegion borderTopLeftRegion;
	public TextureRegion borderTopRightRegion;
	public TextureRegion brewengineRegion;
	public TextureRegion libgdxRegion;
	public TextureRegion logoRegion;
	public TextureRegion menuFontRegion;

	public void load() {
		textures = new TextureAtlas(Gdx.files.internal("menu/textures.txt"));

		borderBottomCenterRegion = textures.findRegion(BORDER_BOTTOM_CENTER);
		borderBottomLeftRegion = textures.findRegion(BORDER_BOTTOM_LEFT);
		borderBottomRightRegion = textures.findRegion(BORDER_BOTTOM_RIGHT);
		borderMiddleCenterRegion = textures.findRegion(BORDER_MIDDLE_CENTER);
		borderMiddleLeftRegion = textures.findRegion(BORDER_MIDDLE_LEFT);
		borderMiddleRightRegion = textures.findRegion(BORDER_MIDDLE_RIGHT);
		borderTopCenterRegion = textures.findRegion(BORDER_TOP_CENTER);
		borderTopLeftRegion = textures.findRegion(BORDER_TOP_LEFT);
		borderTopRightRegion = textures.findRegion(BORDER_TOP_RIGHT);
		brewengineRegion = textures.findRegion(BREWENGINE);
		libgdxRegion = textures.findRegion(LIBGDX);
		logoRegion = textures.findRegion(LOGO);
		menuFontRegion = textures.findRegion(MENU_FONT);

	}
}
