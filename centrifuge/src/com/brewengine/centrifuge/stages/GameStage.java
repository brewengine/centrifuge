package com.brewengine.centrifuge.stages;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.brewengine.centrifuge.actors.Obstacle;
import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.groups.RotatorGroup;

public class GameStage extends Stage {

	public static final float DEFAULT_WIDTH  = 854f;
	public static final float DEFAULT_HEIGHT = 480f;
	
	/**
	 * The group that rotates on the stage based on the accelerometer input.
	 */
	public RotatorGroup rotator;
	
	public Array<ParticleEffect> effects = new Array<ParticleEffect>();
	public final List<Actor> delayedAddActors = new ArrayList<Actor>();
	
	protected final List<Obstacle> obstacles;
	protected final List<Obstacle> immutableObstacles;
	
	private Vector2 tmp = new Vector2();
	
	public GameStage() {
		this(DEFAULT_WIDTH, DEFAULT_HEIGHT, false);
	}
	
	public GameStage(float width, float height, boolean stretch) {
		super(width, height, stretch);
		
		obstacles = new ArrayList<Obstacle>();
		immutableObstacles = Collections.unmodifiableList(obstacles);
		
		rotator = new RotatorGroup();
		rotator.originX = centerX();
		rotator.originY = centerY();
		addActor(rotator);
	}
	
	@Override
	public void act (float delta) {
		for (int i = 0; i < obstacles.size(); i++) {
			Obstacle obstacle = obstacles.get(i);
			if (obstacle.isMarkedToRemove()) {
				obstacles.remove(i);
				i--;
			}
		}
		
		super.act(delta);
		
		for (Actor actor : delayedAddActors) {
			addActor(actor);
		}
		delayedAddActors.clear();
	}
	
	@Override
	public void draw() {
		super.draw();
		drawEffects();
	}

	public void rotatorToStageCoordinates(Vector2 pPoint) {
		pPoint.sub(centerX(), centerY()).rotate(rotator.rotation).add(centerX(), centerY());
	}
	
	public List<Obstacle> getObstacles() {
		return immutableObstacles;
	}
	
	@Override
	public void addActor(Actor actor) {
		if (actor instanceof Obstacle) {
			Obstacle obstacle = (Obstacle) actor;
			obstacles.add(obstacle);
		}
		
		super.addActor(actor);
	}
	
	@Override
	public void removeActor(Actor actor) {
		if (actor instanceof Obstacle) {
			Obstacle obstacle = (Obstacle) actor;
			obstacles.remove(obstacle);
		}
		
		super.removeActor(actor);
	}

	public void addActorLater(Actor pActor) {
		delayedAddActors.add(pActor);
	}
	
	private void drawEffects() {
		float delta = Gdx.graphics.getDeltaTime();
		
		batch.begin();
		
		for (Iterator<ParticleEffect> iterator = effects.iterator(); iterator.hasNext();) {
			ParticleEffect effect = iterator.next();
			if (effect.isComplete()) {
				iterator.remove(); // clean up
			} else {
				effect.draw(batch, delta);
			}
		}
		
		for (Particle particle : rotator.getParticles()) {
			ParticleEffect effect = particle.effect;
			if (effect != null) {
				tmp.set(particle.x, particle.y).add(particle.width / 2, particle.height / 2);
				rotatorToStageCoordinates(tmp);
				effect.setPosition(tmp.x, tmp.y);
				effect.draw(batch, delta);
			}
		}
		
		batch.end();
	}

}
