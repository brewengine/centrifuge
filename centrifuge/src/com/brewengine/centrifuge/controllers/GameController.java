package com.brewengine.centrifuge.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.FadeIn;
import com.badlogic.gdx.scenes.scene2d.actions.MoveTo;
import com.badlogic.gdx.scenes.scene2d.actions.Parallel;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleTo;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.actors.Label;
import com.badlogic.gdx.scenes.scene2d.interpolators.AccelerateInterpolator;
import com.badlogic.gdx.scenes.scene2d.interpolators.DecelerateInterpolator;
import com.brewengine.centrifuge.AssetsGame;
import com.brewengine.centrifuge.CentrifugeGame;
import com.brewengine.centrifuge.actors.Obstacle;
import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.actors.Particle.Type;
import com.brewengine.centrifuge.helpers.CollisionDetector;
import com.brewengine.centrifuge.helpers.CollisionDetector.CollisionDetectorListener;
import com.brewengine.centrifuge.levels.Level;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.models.ParticleLoader;
import com.brewengine.centrifuge.stages.GameStage;

public class GameController implements CollisionDetectorListener, OnActionCompleted {

	public Level level;
	public final GameStage stage;
	public CollisionDetector collisionDetector;
	
	public int score = 0;
	public float gameTime;
	public boolean gameOver;
	public boolean isPaused;
	public boolean didCollideOnce;
	
	private ParticleEffectPool explosions; // TODO move to screen or stage
	
	/**
	 * The diagonal distance from a corner to the center of the stage.
	 */
	private float diagonal;
	private int streak;
	private int bestStreak = 0;
	
	public GameController(GameStage pStage, Level pLevel) {
		collisionDetector = new CollisionDetector(this);
//		collisionDetector.debug = true;
		
		stage = pStage;
		level = pLevel;
		setupLevel();
		
		ParticleEffect explosion = new ParticleEffect();
		explosion.load(Gdx.files.internal("game/explosion.p"), AssetsGame.getInstance().textures);
		explosions = new ParticleEffectPool(explosion, 6, 12);
		
		diagonal = new Vector2().dst(stage.centerX(), stage.centerY());
	}

	private void setupLevel() {
		Particle.sequence = 0;
		Obstacle.sequence = 0;
		
		level.setup(stage.width(), stage.height());
		
		for (ObstacleLoader o : level.obstacles) {
			makeObstacle(o.position, o.type);
		}
	}

	public void update(float pDelta) {
		gameTime += pDelta;
		
		collisionDetector.check();
		
		for (ParticleLoader particle : level.particles)
		{
			if ( particle.time < gameTime && !particle.isDeployed )
			{
				particle.isDeployed = true;
				launchParticle(particle.direction, particle.type);
			}
		}
	}
	
	public Image makeObstacle(Vector2 pPosition, Obstacle.Type pType) {
		Obstacle obstacle = new Obstacle(pType);
		obstacle.enableFade();
		
		if (pType == Obstacle.Type.Goal) {
			obstacle.color.set( 0f, 0f, 1f, 1f );
		} else {
			obstacle.color.set( 0f, 1f, 1f, 1f );
		}
		
		if (collisionDetector.debug) {
			obstacle.color.a = 0.25f;
		}
		
		obstacle.x = stage.centerX() + pPosition.x - obstacle.width / 2;
		obstacle.y = stage.centerY() + pPosition.y - obstacle.height / 2;
		
		stage.addActorLater(obstacle);
		return obstacle;
	}
	
	public void launchParticle(float pDirection, Type type) {
		Particle particle = new Particle(type);
		Color color = particle.color;
		particle.effect.getEmitters().get(0).getTint().setColors(new float[] { color.r, color.g, color.b });
		
		if (collisionDetector.debug) {
			particle.color.a = 0.25f;
		}
		
		particle.x = stage.centerX() - particle.width / 2;
		particle.y = stage.centerY() - particle.height / 2;
		stage.rotator.addActor(particle);
		
		Vector2 target = targetPoint(pDirection);
		int duration = 10;
		
		color.a = 0f;
		particle.action(
			FadeIn.$(0.5f)
		);
		
		particle.action(
			MoveTo.$(target.x, target.y, duration).setInterpolator(
				AccelerateInterpolator.$()
			).setCompletionListener(this)
		);
	}
	
	protected Vector2 targetPoint(float pDirection) {
		float x = stage.centerX() + MathUtils.cosDeg(pDirection) * diagonal;
		float y = stage.centerY() + MathUtils.sinDeg(pDirection) * diagonal;
		return new Vector2(x, y);
	}

	@Override
	public void collision(Particle pParticle, Obstacle pObstacle) {
//		Gdx.app.log("GameController.collision", "particle = " + pParticle + ", obstacle = " + pObstacle);
		if ( pObstacle.isMarkedToRemove() )
		{
			checkForFinish();
			return;
		}
		
		// momentary continuation of particle's effect
		if (pParticle.effect != null) {
			pParticle.effect.setDuration(200);
			stage.effects.add(pParticle.effect);
		}

		Vector2 position = new Vector2(pParticle.x, pParticle.y);
		position.add(pParticle.width / 2, pParticle.height / 2);
		stage.rotatorToStageCoordinates(position);
		
		if ( pParticle.type == Particle.Type.Remove && pObstacle.type != Obstacle.Type.Goal)
//		if ( pParticle.type == Particle.Type.Remove && !pParticle.isMarkedToRemove() && pObstacle.type != Obstacle.Type.Goal)
		{
//			Gdx.app.log("GameController.collision", "Removing obstacle: " + pObstacle.name + " (is marked for removal: " + pObstacle.isMarkedToRemove() + ")");
//			Gdx.app.log("GameController.collision", "Removing particle: " + pParticle.name + " (is marked for removal: " + pParticle.isMarkedToRemove() + ")");
			explodeAndRemove(pObstacle, position);
			explodeAndRemove(pParticle, position);
			checkForFinish();
			return;
		}
		else
		{
			explodeAndRemove(pParticle, position);
		}
		
		Vector2 newObstaclePosition = position.tmp().sub(stage.centerX(), stage.centerY());
		if ( pParticle.type == Type.Cancerous && pObstacle.type != Obstacle.Type.Goal )
		{
			Vector2 vector2 = new Vector2(20, 0);
			// Add 5 obstacles around the collision
			for (int i = 0; i <= 360; i+= 360/5)
			{
				Vector2 newCancerousPosition = vector2.cpy().rotate(i).add(newObstaclePosition);
				makeNewConvertedObstacle(pObstacle, position, newCancerousPosition);
			}
		}
		
		if (pObstacle.type == Obstacle.Type.Standard || pObstacle.type == Obstacle.Type.Converted) {
//			score -= 1;
			
			Gdx.input.vibrate(100);
			
			makeNewConvertedObstacle(pObstacle, position, newObstaclePosition);
		} else if (pObstacle.type == Obstacle.Type.Goal) {

			score += 1 + streak;
			streak += 1;
			
			if ( streak > bestStreak )
			{
				bestStreak = streak;
			}
			// TODO switch these when ready to release to bump out testing scores in open feint
//			streak += 10;
//			score += 10 + streak;
		}
		
		if ( pObstacle.type != Obstacle.Type.Goal )
		{
			didCollideOnce = true;
		}
		
		checkForFinish();
	}

	private void makeNewConvertedObstacle(Obstacle pObstacle, Vector2 positionInParticleSpace,
			Vector2 obstaclePosition)
	{
		Image obstacle = makeObstacle(obstaclePosition, Obstacle.Type.Converted);
		obstacle.color.set(pObstacle.color);
		streak = 0;
		
		if ( obstaclePosition.y > stage.height()/2 || obstaclePosition.y < -stage.height()/2 || obstaclePosition.x > stage.width()/2 || obstaclePosition.x < -stage.width()/2)
		{
			CentrifugeGame.activityCallback.postOutOfSight();
		}
		
		if ( positionInParticleSpace.dst(stage.centerX(), stage.centerY() )  < 10  )
		{
			gameOver();
		}
	}

	private void explodeAndRemove(Image thingy, Vector2 position)
	{
		thingy.markToRemove(true);
		
		// explosion effect
		PooledEffect explosion = explosions.obtain();
		explosion.setPosition(position.x, position.y);
		Color c = thingy.color;
		explosion.getEmitters().get(0).getTint().setColors(new float[] { c.r, c.g, c.b });
		stage.effects.add(explosion);
	}

	private void checkForFinish()
	{
//		System.out.println("level.particles.size" + level.particles.size);
//		System.out.println("mParticleSequence" + mParticleSequence);
//		System.out.println("stage.rotator.getActors().size()" + stage.rotator.getActors().size());
		
		if ( stage.rotator.getParticles().size() == 1 )
		{
			System.out.println( stage.rotator.getParticles().get(0));
		}

		boolean allParticlesLaunched = (level.particles.size == Particle.sequence);
		boolean allParticlesMarkedForRemoval = true;
		
		for (Actor particle : stage.rotator.getParticles())
		{
			if (!particle.isMarkedToRemove()) {
				allParticlesMarkedForRemoval = false;
			}
		}
		
		if ( allParticlesLaunched && (stage.rotator.getParticles().size() == 0 || allParticlesMarkedForRemoval) )
		{
			System.out.println( "Fin! score:" + score );
			gameOver();
		}
	}

	private void gameOver()
	{
		if ( gameOver )
		{
			return;
		}
		gameOver = true;
		Gdx.app.log("GameController.gameOver", "score: " + score);
		
		// report score
		CentrifugeGame.activityCallback.postScore(level.identifier, score);
		CentrifugeGame.activityCallback.postStreak(bestStreak);
		if ( !didCollideOnce )
		{
			CentrifugeGame.activityCallback.postNoHitter();
		}
		if ( noObstaclesLeft() )
		{
			CentrifugeGame.activityCallback.postBreakerBreaker();
		}
		// show "Game over" and menu
		
		BitmapFont font = new BitmapFont();
		font.setColor(Color.WHITE);
		
		Group container = new Group();
		container.originX = stage.centerX();
		container.originY = stage.centerY();
		container.scaleX = 0.1f;
		container.scaleY = 0.1f;
		container.rotation = -180f;
		
		Label label = new Label("Game Over", font, "Game Over");
		label.x = stage.centerX() - label.width /2;
		label.y = stage.centerY() - label.height/2;
		container.addActor(label);
		
		stage.addActor(container);
		
		float duration = 0.8f;
		RotateTo rotate = RotateTo.$(360 * 1f, duration );
		rotate.setInterpolator(DecelerateInterpolator.$());
		ScaleTo expand = ScaleTo.$(3, 3, duration);
		expand.setInterpolator(DecelerateInterpolator.$());
		container.action(Parallel.$(rotate, expand));
		
	}
	
	private boolean noObstaclesLeft()
	{
		for (Obstacle obstacle : stage.getObstacles())
		{
			if ( obstacle.type != Obstacle.Type.Goal && !obstacle.isMarkedToRemove() )
			{
				return false;
			}
		}
		
		return true;
	}

	@Override
	public void completed(Action action) {
		Actor actor = action.getTarget();
		Gdx.app.log("GameController.completed", "Removing actor: " + actor);
		actor.remove();
		
		checkForFinish();
	}

}
