package com.brewengine.centrifuge;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetsGame extends AssetsGameGenerated {

	public TextureRegion[] tiltIncorrect;
	public TextureRegion[] tiltCorrect;
	
	private static AssetsGame instance = null;
	
	protected AssetsGame() {
		// defeat instantiation
	}
	
	public static AssetsGame getInstance() {
		if (instance == null) {
			instance = new AssetsGame();
			instance.load();
		}
		return instance;
	}
	
	public void load() {
		super.load();
		
		tiltIncorrect = new TextureRegion[6];
		tiltIncorrect[0] = orientationNormalRegion;
		tiltIncorrect[1] = orientationFlat2Region;
		tiltIncorrect[2] = orientationFlat1Region;
		tiltIncorrect[3] = orientationFlat0Region;
		tiltIncorrect[4] = orientationFlat1Region;
		tiltIncorrect[5] = orientationFlat2Region;
		
		tiltCorrect = new TextureRegion[4];
		tiltCorrect[0] = orientationLeftRegion;
		tiltCorrect[1] = orientationNormalRegion;
		tiltCorrect[2] = orientationRightRegion;
		tiltCorrect[3] = orientationNormalRegion;
		
		// configure texture filtering
		particleRegion.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}
	
}
