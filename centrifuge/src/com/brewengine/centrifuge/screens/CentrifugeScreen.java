package com.brewengine.centrifuge.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.brewengine.centrifuge.CentrifugeGame;

public abstract class CentrifugeScreen extends InputAdapter implements Screen {
	
	public final Screen parent;
	
	public CentrifugeScreen(Screen pParent) {
		parent = pParent;
		Gdx.app.log("CentrifugeScreen.consructor", "parent: " + parent + " on " + this);
	}
	
	@Override
	public abstract void render(float delta);

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.app.log("CentrifugeScreen.show", "Showing " + this);
		Gdx.input.setInputProcessor(this);
		Gdx.app.log("CentrifugeScreen.show", "Input processor set to: " + this);
	}

	@Override
	public void hide() {
		Gdx.app.log("CentrifugeScreen.hide", "Hiding " + this);
	}

	@Override
	public void pause() {
		Gdx.app.log("CentrifugeScreen.pause", "Pausing " + this);
	}

	@Override
	public void resume() {
		Gdx.app.log("CentrifugeScreen.resume", "Resuming " + this);
	}

	@Override
	public void dispose() {
		Gdx.app.log("CentrifugeScreen.dispose", "Disposing " + this);
	}
	
	@Override
	public boolean keyUp(int keycode) {
		Gdx.app.log("CentrifugeScreen.keyUp", "keycode: " + keycode);
		
		switch (keycode) {
		case Keys.BACK:
		case Keys.BACKSPACE:
		case Keys.ESCAPE:
			Gdx.app.log("CentrifugeScreen.keyUp", "parent: " + parent);
			if (parent != null) {
				Gdx.app.log("CentrifugeScreen.keyUp", "Setting parent screen.");
				CentrifugeGame.instance.setScreen(parent);
			}
			return true;
		}
		
		return false;
	}
	
}
