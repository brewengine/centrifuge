package com.brewengine.centrifuge.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.FadeTo;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.ui.tablelayout.Table;
import com.brewengine.centrifuge.AssetsMenu;
import com.brewengine.centrifuge.CentrifugeGame;
import com.brewengine.centrifuge.actors.LinkImage;
import com.brewengine.centrifuge.helpers.ScaleHelper;
import com.brewengine.centrifuge.ui.MenuList;

public abstract class MenuScreen extends InputAdapter implements Screen {

	public static boolean disposed = true;
	
	public final Screen parent;
	
	public Image background;
	public BitmapFont font; // default font
	public Stage stage;
	public Table view; // main view of the menu screen
	public MenuList menu;
	
	private MenuScreen nextScreen;

	private Image logo; // Centrifuge logo
	private Image libgdx; // powered by libGDX logo
	private LinkImage brewengine; // Brew Engine logo
	
	public MenuScreen() {
		this(null);
	}
	
	public MenuScreen(Screen pParent) {
		parent = pParent;
//		Group.enableDebugging("menu/debug.png");
	}
	
	public void setMenu(MenuScreen pScreen) {
		Gdx.app.log("MenuScreen.setScreen", "Reusing assets.");
		pScreen.font = this.font;
		
		nextScreen = pScreen;
		setScreen(pScreen);
	}
	
	public void setScreen(Screen pScreen) {
		if (pScreen == null) {
			// TODO throw exception, cannot set screen to null
			return;
		}
		
		CentrifugeGame.instance.setScreen(pScreen);
	}
	
	public void back() {
		if (parent != null) {
			if (parent instanceof MenuScreen) {
				setMenu((MenuScreen) parent);
			} else {
				setScreen(parent);
			}
		}
	}
	
	protected abstract void setupMenu();
	
	protected void loadAssets() {
		if (disposed) {
			Gdx.app.log("MenuScreen.loadAssets", "Assets were disposed, reloading.");
			disposed = false;
			font = null;
			AssetsMenu.getInstance().load();
		}
		
		if (font == null) {
			FileHandle fontFile = Gdx.files.internal("menu/menu.fnt");
			TextureRegion region = AssetsMenu.getInstance().menuFontRegion;
			boolean flip = false;
			font = new BitmapFont(fontFile, region, flip);
		}
		
//		if (itemBackground == null) {
//			itemBackground = new Image("item background", Assets_menu.itembackgroundRegion);
//		}
	}
	
	protected void setupStage() {
		boolean stretch = false;
		stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), stretch);
		
		logo = new Image("centrifuge logo", AssetsMenu.getInstance().logoRegion);
		logo.action(
			Forever.$(
				Sequence.$(
					FadeTo.$(0.5f, 0.5f),
					FadeTo.$(1f, 0.5f)
				)
			)
		);
		
		libgdx = new Image("libgdx logo", AssetsMenu.getInstance().libgdxRegion);
		libgdx.scaleX = 0.85f;
		libgdx.scaleY = 0.85f;
		
		brewengine = new LinkImage("brewengine logo", AssetsMenu.getInstance().brewengineRegion);
		brewengine.scaleX = 0.75f;
		brewengine.scaleY = 0.75f;
		
		view = new Table("view");
		view.transform = true;
		view.row();
		view.add(logo).colspan(2);
		view.row();
		view.add(menu).space(20, 0, 20, 0).colspan(2);
		view.row();
		view.add(brewengine).center();
		view.add(libgdx).space(10, 0, 0, 15);
//		view.debug();
		
		stage.addActor(view);
	}
	
	protected void setupBackground() {
//		TextureRegion region = Assets_menu.backgroundRegion;
//		background = new Image("background", region);
		
		// set background to scale from it's bottom left corner
//		background.originX = 0f;
//		background.originY = 0f;
		
//		stage.addActor(background);
	}

	protected void layout() {
		view.x = stage.width() / 2;
		view.y = stage.height() / 2;
		
		ScaleHelper.scaleToFit(view, stage.width(), stage.height());
		
//		float backgroundScale = ScaleHelper.scaleToFill(background, stage.width(), stage.height());
//		Gdx.app.log("MenuScreen.layout", "backgroundScale: " + backgroundScale);
		
		// have background stick to top left corner of stage
//		background.x = 0f;
//		background.y = stage.height() - background.height * backgroundScale;
		
		// center background on the stage
//		background.x = stage.width() / 2  - background.width  / 2;
//		background.y = stage.height() / 2 - background.height / 2;
	}
	
	private void setup() {
		nextScreen = null;
		loadAssets();
		
		setupBackground();
		setupMenu();
		setupStage();
		
		layout();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
		Table.drawDebug(stage);
	}
	
	@Override
	public void resize(int width, int height) {
		boolean strech = false;
		stage.setViewport(width, height, strech);
		layout();
	}

	@Override
	public void show() {
		Gdx.app.log("MenuScreen.show", "this: " + this);
		setup();
		Gdx.input.setInputProcessor(this);
	}

	@Override
	public void hide() {
		Gdx.app.log("MenuScreen.hide", "this: " + this);
		
		if (nextScreen != null && nextScreen instanceof MenuScreen) {
			// don't dispose as we will reuse assets on next screen
			((MenuScreen) nextScreen).font = font;
		} else {
			Gdx.app.log("MenuScreen.setScreen", "Disposing assets.");
			disposed = true;
			dispose();
		}
	}

	@Override
	public void pause() {
		Gdx.app.log("MenuScreen.pause", "this: " + this);
	}

	@Override
	public void resume() {
		Gdx.app.log("MenuScreen.resume", "this: " + this);
		loadAssets();
	}

	@Override
	public void dispose() {
		Gdx.app.log("MenuScreen.dispose", "this: " + this);
		stage.dispose();
		font.dispose();
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return stage.touchDown(x, y, pointer, button);
	}

	@Override
	public boolean touchMoved (int x, int y) {
		return stage.touchMoved(x, y);
	}

	@Override
	public boolean touchDragged(int x, int y, int pointer) {
		return stage.touchDragged(x, y, pointer);
	}

	@Override
	public boolean touchUp (int x, int y, int pointer, int button) {
		return stage.touchUp(x, y, pointer, button);
	}

//	@Override
//	public boolean keyTyped(char character)
//	{
//		List<Actor> actors = menu.getActors();
//		return keyTyped(character, actors);
//	}

//	private boolean keyTyped(char character, List<Actor> actors)
//	{
//		for (Actor actor : actors)
//		{
//			if (actor.name.charAt(0) == Character.toUpperCase(character))
//			{
//				touched(actor);
//				return true;
//			}
//			else
//			{
//				if ( actor instanceof MenuGroup )
//				{
//					return keyTyped(character, ((MenuGroup) actor).getActors());
//				}
//			}
//		}
//		return super.keyTyped(character);
//	}
	
	@Override
	public boolean keyUp( int keycode )
	{
		switch (keycode)
		{
		case Keys.BACK: // android 'back'
		case Keys.DEL: // delete or backspace
		case Keys.ESCAPE:
			back();
			return true;
		}
		
		return false;
	}
	
	public interface IMenuTransition {
		public void run(MenuScreen pScreen);
	}
	
}
