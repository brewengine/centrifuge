package com.brewengine.centrifuge.screens.menus;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List.SelectionListener;
import com.brewengine.centrifuge.AssetsMenu;
import com.brewengine.centrifuge.CentrifugeGame;
import com.brewengine.centrifuge.screens.LevelEditorScreen;
import com.brewengine.centrifuge.screens.LevelSelectorScreen;
import com.brewengine.centrifuge.screens.MenuScreen;
import com.brewengine.centrifuge.ui.MenuList;
import com.brewengine.centrifuge.ui.MenuList.TouchListener;

public class MainMenuScreen extends MenuScreen implements SelectionListener, TouchListener {

	static final Color COLOR_SELECTED   = new Color(  2 / 255f, 153 / 255f, 208 / 255f, 1f);
	static final Color COLOR_UNSELECTED = new Color(204 / 255f, 236 / 255f,         1f, 1f);
	
	static final String NEW_GAME     = "New Game";
	static final String LEVEL_EDITOR = "Level Editor";
	static final String QUIT         = "Quit";
	static final String SCORES       = "Scores";

	@Override
	protected void setupMenu() {
		String items[];
		if (Gdx.app.getType().equals(ApplicationType.Desktop)) {
			items = new String[] {
				NEW_GAME,
				LEVEL_EDITOR,
				QUIT,
			};
		} else {
			items = new String[] {
				NEW_GAME,
				SCORES,
				QUIT,
			};
		}
		
		NinePatch selectedPatch = new NinePatch(AssetsMenu.getInstance().patches);
		ListStyle style = new ListStyle(font, COLOR_SELECTED, COLOR_UNSELECTED, selectedPatch);
		menu = new MenuList(items, style, "menu");
		menu.setSelectionListener(this);
		menu.setTouchListener(this);
	}

	@Override
	public void selected(List list, int selectedIndex, String selection) {
		
	}
	
	@Override
	public void touchUp(List list, int selectedIndex, String selection) {
		Screen screen = null;
		
		if (selection.equals(NEW_GAME)) {
			screen = new LevelSelectorScreen(this);
		} else if (selection.equals(LEVEL_EDITOR)) {
			screen = new LevelEditorScreen(this);
		} else if (selection.equals(SCORES)) {
			CentrifugeGame.activityCallback.showScores();
		} else {
			Gdx.app.exit();
			return;
		}
		
		setScreen(screen);
	}
	
	@Override
	public boolean keyUp(int keycode) {
		Gdx.app.log("MainMenuScreen.keyUp", "keycode = " + keycode);
		
		int change = 0;
		switch (keycode)
		{
		case Keys.UP:
			change = -1;
			break;
		case Keys.DOWN:
			change = 1;
			break;
		case Keys.ENTER:
			touchUp(menu, menu.getSelectedIndex(), menu.getSelection());
			return true;
		case Keys.ESCAPE:
		case Keys.BACKSPACE:
		case Keys.BACK:
			Gdx.app.exit();
			return true;
		}
		
		int index = menu.getSelectedIndex() + change;
		int length = menu.getItems().length - 1;
		if (index < 0) {
			index = length;
		} else if (index > length) {
			index = 0;
		}
		menu.setSelectedIndex(index);
		
		if (change != 0) {
			return true;
		} else {
			return super.keyUp(keycode);
		}
	}

}
