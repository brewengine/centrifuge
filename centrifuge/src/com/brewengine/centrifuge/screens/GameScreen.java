package com.brewengine.centrifuge.screens;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Peripheral;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actors.Label;
import com.brewengine.centrifuge.AssetsGame;
import com.brewengine.centrifuge.CentrifugeGame;
import com.brewengine.centrifuge.controllers.GameController;
import com.brewengine.centrifuge.helpers.AccelerometerHelper;
import com.brewengine.centrifuge.helpers.MathHelper;
import com.brewengine.centrifuge.helpers.SmoothAccelerometer;
import com.brewengine.centrifuge.helpers.TiltIndicator;
import com.brewengine.centrifuge.helpers.controls.Accelerometer;
import com.brewengine.centrifuge.helpers.controls.ArrowKeys;
import com.brewengine.centrifuge.helpers.controls.Controls;
import com.brewengine.centrifuge.levels.Level;
import com.brewengine.centrifuge.stages.GameStage;

public class GameScreen extends CentrifugeScreen {

	public GameStage stage;
	public GameController gameController;
	
	private boolean disposed = true;
	private Controls controls;
	
	private int score;
	public BitmapFont font;
	private Label scoreLabel;
	private Label scoreLabel180;
	private final Level selectedLevel;
	
	private float timePassed;
	private int totalFPS;
	private int totalFPSCount;
	private int maxFPS = Integer.MIN_VALUE;
	private int minFPS = Integer.MAX_VALUE;
	private float overallRotation;
	
	private TiltIndicator tiltIndicator;
	
	public GameScreen(Screen pParent, Level selectedLevel) {
		super(pParent);
		this.selectedLevel = selectedLevel;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		
		// collect fps data for flurry
		timePassed += delta;
		if (timePassed > 1f) {
			timePassed = 0f;
			int fps = Gdx.graphics.getFramesPerSecond();
			
			if (fps > maxFPS ) {
				maxFPS = fps;
			} else if (fps < minFPS ) {
				minFPS = fps;
			}
			
			totalFPS += fps;
			totalFPSCount++;
		}
		
		if (Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer)) {
			tiltIndicator.update(delta);
			AccelerometerHelper.update(); // used to collect data for flurry reporting
		}
		
		if (!gameController.gameOver && !gameController.isPaused) {
			controls.update(delta);
			
			float rotation = controls.getRotation();
			achievementCheck(rotation);
			stage.rotator.rotation = rotation;
			gameController.update(delta);
			
			if (score != gameController.score) {
				score = gameController.score;
				updateScore(score);
			}
		}
		
		stage.draw();
	}

	private void achievementCheck(float pRotation) {
		if ( pRotation != stage.rotator.rotation )
		{
			float diff = MathHelper.differenceOfAnglesDegrees(pRotation, stage.rotator.rotation);
			if (Math.abs(diff) > 90f) { // ignore possible flipping of the phone on an axis
				return;
			}
			
			overallRotation += diff;
			if ( Math.abs(overallRotation) > 720 )
			{
				overallRotation = 0;
				CentrifugeGame.activityCallback.post720();
			}
		}
	}

	@Override
	public void show() {
		super.show();
		setup();
	}
	
	@Override
	public void hide() {
		super.hide();
		
		try {
			Gdx.app.log("GameScreen.hide", "Submitting Flurry data.");
			
			// submit data to flurry
			HashMap<String, String> parameters = new HashMap<String, String>();
			
			parameters.putAll( CentrifugeGame.activityCallback.buildDetails() );
			
			parameters.put("level", "" + gameController.level.identifier);
			parameters.put("resolution", "" + Gdx.graphics.getWidth() + "x" + Gdx.graphics.getHeight());
			parameters.put("game duration", "" + Math.round( gameController.gameTime ));
			parameters.put("average accelerometer updates per second", "" + Math.round( AccelerometerHelper.getAverageResolution() ));
			
			parameters.put("max FPS", "" + maxFPS);
			parameters.put("average FPS", "" + Math.round( totalFPS / (float) totalFPSCount ));
			
			CentrifugeGame.activityCallback.flurryEvent("game end", parameters);
		} catch (Exception e) {
			Gdx.app.error("GameScreen.hide", "Failed to post Flurry event.", e);
		}
	}
	
	@Override
	public void dispose() {
		disposed = true;
		stage.dispose();
	}

	private void setup() {
		loadAssets();
		setupInput();
		setupStage();
		gameController = new GameController(stage, selectedLevel);
	}

	private void loadAssets() {
		if (disposed) {
			Gdx.app.log("GameScreen.loadAssets", "Assets were disposed, reloading.");
			disposed = false;
			font = null;
			AssetsGame.getInstance().load();
		}
		
		if (font == null) {
			FileHandle fontFile = Gdx.files.internal("game/text.fnt");
			TextureRegion region = AssetsGame.getInstance().textRegion;
			boolean flip = false;
			font = new BitmapFont(fontFile, region, flip);
			font.setColor(Color.WHITE);
			font.setScale(1.5f);
		}
	}
	
	private void setupInput() {
		SmoothAccelerometer.filteringFactor = 0.8f;
		
		if (Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer)) {
			controls = new Accelerometer();
		} else {
			ArrowKeys arrows = new ArrowKeys();
			controls = arrows;
			Gdx.input.setInputProcessor(new InputMultiplexer(arrows, this));
		}
	}
	
	private void setupStage() {
		Gdx.app.log("GameScreen.setupStage", "Setting up stage.");
		stage = new GameStage();
		
		scoreLabel = new Label("score", font, "0");
		stage.addActor(scoreLabel);
		
		Group container = new Group();
		container.originX = stage.centerX();
		container.originY = stage.centerY();
		container.rotation = 180f;
		scoreLabel180 = new Label("score180", font, "0");
		container.addActor(scoreLabel180);
		stage.addActor(container);
		
		updateScore(0);
		
		tiltIndicator = new TiltIndicator();
		// align to upper right corner of stage
		tiltIndicator.group.x = stage.right() - tiltIndicator.group.width;
		tiltIndicator.group.y = stage.top() - tiltIndicator.group.height;
		stage.addActor(tiltIndicator.group);
	}
	
	private void updateScore(int pScore) {
		scoreLabel.setText("" + pScore);
		scoreLabel.x = stage.width() - scoreLabel.width - 5f;
		scoreLabel.y = 5f;
		
		scoreLabel180.setText("" + pScore);
		scoreLabel180.x = scoreLabel.x;
		scoreLabel180.y = scoreLabel.y;
	}
	
//	public boolean keyUp(int keycode) {
//		OrthographicCamera camera = (OrthographicCamera) stage.getCamera();
//		
//		switch (keycode) {
//		case Keys.COMMA:
//			camera.zoom += 0.1f; 
//			return true;
//		case Keys.PERIOD:
//			camera.zoom -= 0.1f;
//			return true;
//		case Keys.I:
//			System.out.println("gameOver?" + gameController.gameOver);
//			System.out.println("isPaused?" + gameController.isPaused);
//			System.out.println("stage.rotator.getActors().size()?" + stage.rotator.getActors().size());
//			System.out.println("particles loaded: " + gameController.level.particles );
//		}
//		
//		return super.keyUp(keycode);
//	}
	
}
