package com.brewengine.centrifuge.screens;

import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.brewengine.centrifuge.AssetsGame;
import com.brewengine.centrifuge.actors.Obstacle;
import com.brewengine.centrifuge.helpers.GridRenderer;
import com.brewengine.centrifuge.models.Grid;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.stages.GameStage;

public class LevelEditorScreen extends CentrifugeScreen
{

	private static final int VIEW = 2;
	private static final int PLACING_OBSTACLE = 1;
	private static final int PLACING_OBSTACLE_MIRROR_V = 3; // vertical
	private static final int PLACING_OBSTACLE_MIRROR_H = 4; // horizontal
	private static final int PLACING_OBSTACLE_MIRROR_D = 5; // diagonal
	private static final int PLACING_OBSTACLE_MIRROR_A = 6; // all
	private static final int PLACING_GOAL = 7;
	private int state = 2;
	
	private boolean disposed = true;
	public GameStage stage;
	private List<Obstacle> currentObstacle = new ArrayList<Obstacle>();
	private BitmapFont font;
	private SpriteBatch spriteBatch;
	private Array<ObstacleLoader> obstacles = new Array<ObstacleLoader>();
	private float snapsize;
	private GridRenderer grid;
	

	public LevelEditorScreen(Screen pParent) {
		super(pParent);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		grid.draw();
		
		stage.act(delta);
		stage.draw();
		
		spriteBatch.begin();
			font.draw(spriteBatch, "Press H", 30, 30);
		spriteBatch.end();
	}

	@Override
	public boolean keyUp(int keycode) {
		switch (keycode) {
		case Keys.H:
			System.out.println("P to start placing new obstacles");
			System.out.println("G to start placing new goals");
			System.out.println("");
			System.out.println("0 reset rotator rotation");
			System.out.println("1 horizontal mirror mode");
			System.out.println("2 vertical mirror mode");
			System.out.println("3 diagonal mirror mode");
			System.out.println("4 all mirror mode");
			System.out.println("");
			System.out.println("C to take a break, eat a hotpocket");
			System.out.println("E to erase/remove");
			System.out.println("D to finish and 'print'");
			
			System.out.println("Back to go back to main menu");
			return true;
		case Keys.P:
			state = PLACING_OBSTACLE;
			return true;
		case Keys.G:
			state = PLACING_GOAL;
			return true;
		case Keys.NUM_0:
			stage.rotator.rotation = 0f;
			return true;
		case Keys.NUM_1:
			state = PLACING_OBSTACLE_MIRROR_H;
			System.out.println("horizontal mirror mode");
			return true;
		case Keys.NUM_2:
			state = PLACING_OBSTACLE_MIRROR_V;
			System.out.println("vertical mirror mode");
			return true;
		case Keys.NUM_3:
			state = PLACING_OBSTACLE_MIRROR_D;
			System.out.println("diagonal mirror mode");
			return true;
		case Keys.NUM_4:
			state = PLACING_OBSTACLE_MIRROR_A;
			System.out.println("all mirror mode");
			return true;
		case Keys.D:
			print();
			state = VIEW;
			return true;
		case Keys.E:
			removeLast();
			break;
		case Keys.C:
			state = VIEW;
			return true;
		case Keys.ESCAPE:
			if (state != VIEW) {
				cancelPlacingObstacle();
				state = VIEW;
				return true;
			}
		}
		
		return super.keyUp(keycode);
	}
	
	private void removeLast()
	{
		if (stage.getActors().isEmpty() )
		{
			return;
		}
		stage.getActors().get(stage.getActors().size()-1).markToRemove(true);
		obstacles.removeIndex(obstacles.size-1);
	}

	private void cancelPlacingObstacle() {
		for (Obstacle obstacle : currentObstacle) {
			obstacle.remove();
		}
		currentObstacle.clear();
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button)
	{
//		y = Gdx.graphics.getHeight() - y;
		
		this.currentObstacle.clear();
		
		switch (state) {
		case PLACING_GOAL:
			this.currentObstacle.add( makeObstacle(new Vector2(), Obstacle.Type.Goal) );
			break;
		case PLACING_OBSTACLE_MIRROR_A:
			this.currentObstacle.add( makeObstacle(new Vector2()) );
			this.currentObstacle.add( makeObstacle(new Vector2()) );
		case PLACING_OBSTACLE_MIRROR_D:
		case PLACING_OBSTACLE_MIRROR_H:
		case PLACING_OBSTACLE_MIRROR_V:
			this.currentObstacle.add( makeObstacle(new Vector2()) );
		case PLACING_OBSTACLE:
			this.currentObstacle.add( makeObstacle(new Vector2()) );
			break;
		default:
			return super.touchDown(x, y, pointer, button);
		}
		
		for (int i = 1; i < this.currentObstacle.size(); i++) {
			Obstacle obstacle = this.currentObstacle.get(i);
			obstacle.color.a = 0.35f;
		}
		
		return touchDragged(x, y, pointer);
	}
	
	@Override
	public boolean touchDragged(int x, int y, int pointer)
	{
		y = Gdx.graphics.getHeight() - y;
		
		if (state == PLACING_OBSTACLE ||
			state == PLACING_GOAL ||
			state == PLACING_OBSTACLE_MIRROR_D ||
			state == PLACING_OBSTACLE_MIRROR_H ||
			state == PLACING_OBSTACLE_MIRROR_V ||
			state == PLACING_OBSTACLE_MIRROR_A)
		{
			x -= this.currentObstacle.get(0).width * 1.25f;
			y += this.currentObstacle.get(0).height / 4;
			
			snapsize = this.currentObstacle.get(0).width / 2;
			float cx = stage.centerX() % snapsize;
			float cy = stage.centerY() % snapsize;
			x = (int) (MathUtils.roundPositive(x / snapsize) * snapsize + cx);
			y = (int) (MathUtils.roundPositive(y / snapsize) * snapsize + cy);
			
			this.currentObstacle.get(0).x = x;
			this.currentObstacle.get(0).y = y;
			
			int rx = (int) (this.currentObstacle.get(0).x - stage.centerX() + this.currentObstacle.get(0).width / 2);
			int ry = (int) (this.currentObstacle.get(0).y - stage.centerY() + this.currentObstacle.get(0).height / 2);
			
			switch (state) {
			case PLACING_OBSTACLE_MIRROR_D:
				this.currentObstacle.get(1).x = stage.centerX() + -rx - this.currentObstacle.get(0).width / 2;
				this.currentObstacle.get(1).y = stage.centerY() + -ry - this.currentObstacle.get(0).height / 2;
				break;
			case PLACING_OBSTACLE_MIRROR_H:
				this.currentObstacle.get(1).x = stage.centerX() + rx - this.currentObstacle.get(0).width / 2;
				this.currentObstacle.get(1).y = stage.centerY() + -ry - this.currentObstacle.get(0).height / 2;
				break;
			case PLACING_OBSTACLE_MIRROR_V:
				this.currentObstacle.get(1).x = stage.centerX() + -rx - this.currentObstacle.get(0).width / 2;
				this.currentObstacle.get(1).y = stage.centerY() + ry - this.currentObstacle.get(0).height / 2;
				break;
			case PLACING_OBSTACLE_MIRROR_A:
				this.currentObstacle.get(1).x = stage.centerX() + -rx - this.currentObstacle.get(0).width / 2;
				this.currentObstacle.get(1).y = stage.centerY() + -ry - this.currentObstacle.get(0).height / 2;
				this.currentObstacle.get(2).x = stage.centerX() + rx - this.currentObstacle.get(0).width / 2;
				this.currentObstacle.get(2).y = stage.centerY() + -ry - this.currentObstacle.get(0).height / 2;
				this.currentObstacle.get(3).x = stage.centerX() + -rx - this.currentObstacle.get(0).width / 2;
				this.currentObstacle.get(3).y = stage.centerY() + ry - this.currentObstacle.get(0).height / 2;
				break;
			}
			
			return true;
		}
		
		return super.touchDragged(x, y, pointer);
	}
	
	@Override
	public boolean touchUp(int x, int y, int pointer, int button)
	{
		y = Gdx.graphics.getHeight() - y;
		
		if ( currentObstacle != null )
		{
			for (Obstacle o : currentObstacle) {
				o.color.a = 1f;
				Vector2 position = new Vector2(o.x - stage.centerX() + o.width / 2, o.y - stage.centerY() + o.height / 2);
				obstacles.add(new ObstacleLoader(position, o.type));
			}
			currentObstacle.clear();
		}
		
		return super.touchUp(x, y, pointer, button);
	}
	
	@Override
	public boolean touchMoved(int x, int y)
	{
		y = Gdx.graphics.getHeight() - y;
		
		return super.touchMoved(x, y);
	}
	
	private void print() {
		Writer writer = new StringWriter();
		Json json = new Json();
		
		try {
			json.toJson(obstacles, writer);
			System.out.println(writer);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private Obstacle makeObstacle(Vector2 pPosition) {
		return makeObstacle(pPosition, Obstacle.Type.Standard);
	}
	
	private Obstacle makeObstacle(Vector2 pPosition, Obstacle.Type type) {
		Obstacle obstacle = new Obstacle(type);
		obstacle.x = stage.centerX() + pPosition.x - obstacle.width / 2;
		obstacle.y = stage.centerY() + pPosition.y - obstacle.height / 2;
		stage.addActor(obstacle);
		
		return obstacle;
	}
	
	@Override
	public void show() {
		super.show();
		setup();
	}
	
	@Override
	public void dispose() {
		disposed = true;
		stage.dispose();
	}

	private void setup() {
		setupStage();
		
		Obstacle o = makeObstacle(new Vector2(0f, 0f));
		o.color.a = 0.2f;
		
		grid = new GridRenderer(new Grid(o.width * 1.5f, o.height * 1.5f, stage.centerX(), stage.centerY()), new Rectangle(0, 0, stage.width(), stage.height()));
		grid.color.set(new Color(0.1f, 0.1f, 0.1f, 1f));

		font = new BitmapFont();
		font.setColor(Color.WHITE);

		spriteBatch = new SpriteBatch();		
	}
	
	private void setupStage() {
		Gdx.app.log("GameScreen.setupStage", "Setting up stage.");
		
		if (disposed) {
			disposed = false;
			Gdx.app.log("GameScreen.setupStage", "Loading assets.");
			AssetsGame.getInstance().load();
		}
		
		stage = new GameStage();
		
//		Gdx.app.log("LevelEditorScreen.setupStage", "Gdx.graphics.getWidth() = " + Gdx.graphics.getWidth() + ", Gdx.graphics.getHeight() = " + Gdx.graphics.getHeight());
//		Gdx.app.log("LevelEditorScreen.setupStage", "stage.width() = " + stage.width() + ", stage.height() = " + stage.height());
	}
}
