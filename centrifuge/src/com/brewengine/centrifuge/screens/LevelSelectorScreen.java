package com.brewengine.centrifuge.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.OnActionCompleted;
import com.badlogic.gdx.scenes.scene2d.actions.RotateTo;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.actors.Label;
import com.badlogic.gdx.scenes.scene2d.interpolators.OvershootInterpolator;
import com.brewengine.centrifuge.AssetsGame;
import com.brewengine.centrifuge.CentrifugeGame;
import com.brewengine.centrifuge.actors.Obstacle;
import com.brewengine.centrifuge.controllers.GameController;
import com.brewengine.centrifuge.helpers.MathHelper;
import com.brewengine.centrifuge.levels.Level;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.stages.GameStage;

public class LevelSelectorScreen extends CentrifugeScreen implements GestureListener{

	public static final int LEVEL_COUNT = 10;
	
	public static final String LEVEL = "Level ";
	public static final String BEST_SCORE = "Your best: ";
	public static final String FRIEND_SCORE = "Friend's best: ";
	public static final String PARTICLES = "Particles: ";

	private static final int LEVELS_AVAILABLE_IN_LITE_VERSION = 4;
	
	public GameStage stage;
	public GameController gameController;
	
	private boolean disposed = true;
	
	public BitmapFont font;
	private Group biggestGroup;
	private static int selectedLevel = 1;

	private Label levelLabel;
	private Label scoreLabel;
	private Label friendLabel;
	private Label numberOfParticlesLabel;
	private Integer[] possibleScores;
	private Image friendImage;
	
	public LevelSelectorScreen(Screen pParent) {
		super(pParent);
	}
	
	public Level getLevel(int number) {
		try {
			Level level = (Level) Class.forName("com.brewengine.centrifuge.levels.Level" + number).newInstance();
			level.identifier = number;
			return level;
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		
		stage.draw();
		
		if ( CentrifugeGame.activityCallback.isInvalid() )
		{
			CentrifugeGame.activityCallback.getLevelScores(selectedLevel, scoreLabel, friendLabel, friendImage);
		}
	}

	private void highlightLevel() {
		for (Group level : biggestGroup.getGroups())
		{
			level.color.a = 0.3f;
		}
		
		Group selected = getClosestLevel(biggestGroup.rotation);
		
		if (selected != null) {
			selected.color.a = 1f;
			
			int level = Integer.parseInt(selected.name.substring( 9 ));
			selectedLevel = level;
			CentrifugeGame.activityCallback.getLevelScores(level, scoreLabel, friendLabel, friendImage);
			levelLabel.setText(LEVEL + level);
			numberOfParticlesLabel.setText(PARTICLES + possibleScores[level-1]);
		}
		else
		{
			selectedLevel = Integer.MIN_VALUE;
			levelLabel.setText("");
			scoreLabel.setText("");
			numberOfParticlesLabel.setText("");
		}
	}

	private Group getClosestLevel(float currentRotation)
	{
		Group selected = null;
		
		float smallestDiff = Float.MAX_VALUE;
		
		for (Group level : biggestGroup.getGroups()) {
			float startAngle = -level.rotation;
			float diff = Math.abs(MathHelper.differenceOfAnglesDegrees(startAngle, currentRotation));
			if ( diff < smallestDiff) {
				selected = level;
				smallestDiff = diff;
			}
		}
		return selected;
	}

	@Override
	public void show() {
		setupInput();
		setup();
	}
	
	private void setupInput() {
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(new GestureDetector(this));
		multiplexer.addProcessor(this);
		Gdx.input.setInputProcessor(multiplexer);
	}
	@Override
	public void dispose() {
		disposed = true;
		stage.dispose();
	}

	private void setup() {

		loadAssets();
		setupStage();
		setupText();
		setupLevels();
	}

	private void loadAssets() {
		if (disposed) {
			Gdx.app.log("LevelSelectorScreen.loadAssets", "Assets were disposed, reloading.");
			disposed = false;
			font = null;
			AssetsGame.getInstance().load();
		}
		
		if (font == null) {
			FileHandle fontFile = Gdx.files.internal("game/text.fnt");
			TextureRegion region = AssetsGame.getInstance().textRegion;
			boolean flip = false;
			font = new BitmapFont(fontFile, region, flip);
			font.setColor(Color.WHITE);
			font.setScale(1.5f);
		}
	}
	
	private void setupStage() {
		Gdx.app.log("GameScreen.setupStage", "Setting up stage.");
		stage = new GameStage();
	}
	
	private void setupLevels() {
		biggestGroup = new Group();
		float scale = 0.6f;
		float idealSeperation = 95 * scale;
		float circumference = idealSeperation * LEVEL_COUNT;
		float radius = circumference / 2f * (float) (Math.PI);
		float yOfBiggest = stage.height() * .65f - radius;
		Vector2 centerOfBiggest = new Vector2(stage.centerX(), yOfBiggest);
//		Vector2 centerOfBiggest = new Vector2(stage.centerX(), -stage.height());
		biggestGroup.originX = centerOfBiggest.x;
		biggestGroup.originY = centerOfBiggest.y;
		float width = stage.width() * scale;
		float height = stage.height() * scale;
		float rotationSeparation = -360/LEVEL_COUNT;
		
		possibleScores = new Integer[LEVEL_COUNT];
		
		for (int i = 1; i <= LEVEL_COUNT; i++) {
			Level level = getLevel(i);
			
			Group levelContainer = new Group("container"+ i);
			Vector2 center = getCenter(centerOfBiggest, i * rotationSeparation, radius);
//			Vector2 center = getCenter(centerOfBiggest, i * rotationSeparation, stage.height()*1.75f);
			levelContainer.originX = width / 2;
			levelContainer.originY = height / 2;
			levelContainer.x = center.x - width/2;
			levelContainer.y = center.y - height /2;
			levelContainer.scaleX = width / GameStage.DEFAULT_WIDTH;
			levelContainer.scaleY = height / GameStage.DEFAULT_HEIGHT;
			levelContainer.color.a = 0.6f;
			
			levelContainer.rotation = i * rotationSeparation;
			
			biggestGroup.addActor(levelContainer);
			
			level.setup(width, height);
			
			for (ObstacleLoader o : level.obstacles) {
				levelContainer.addActor(makeObstacle(o.position, o.type, width, height));
			}
			
			possibleScores[i-1] = level.particles.size;
		}
		
		stage.addActor(biggestGroup);
		
		setRotation(selectedLevel);
	}
	
	private void setRotation(int level)
	{
		int rotateTo = 360/LEVEL_COUNT * level;
		biggestGroup.rotation = rotateTo;
		highlightLevel();
	}
	
	private void setRotationSmooth(int level)
	{
		int rotateTo = 360/LEVEL_COUNT * level;
		rotateToSmooth(rotateTo);
	}

	private void rotateToSmooth(float destination) {
		float dest = biggestGroup.rotation - MathHelper.differenceOfAnglesDegrees(biggestGroup.rotation, destination);
//		Gdx.app.log("LevelSelectorScreen.setRotationSmooth", "from = " + biggestGroup.rotation + ", to = " + dest);
		
		biggestGroup.action(RotateTo.$(dest, .4f).setInterpolator(OvershootInterpolator.$(2f)).setCompletionListener(new OnActionCompleted()
		{
			@Override
			public void completed(Action action)
			{
				highlightLevel();
			}
		}));
	}

	private void setupText() {
		levelLabel = new Label(LEVEL + " 00", font, LEVEL + " 00");
		scoreLabel = new Label("score", font, BEST_SCORE);
		friendLabel = new Label("friendScore", font, FRIEND_SCORE);
		numberOfParticlesLabel = new Label("particleCount", font, PARTICLES);
		
		Group textGroup = new Group();
		
		textGroup.addActor(levelLabel);
		textGroup.addActor(scoreLabel);
		textGroup.addActor(friendLabel);
		textGroup.addActor(numberOfParticlesLabel);
		levelLabel.y = scoreLabel.height * 1.5f;
		friendLabel.y = scoreLabel.height * -1.5f;
		numberOfParticlesLabel.y = scoreLabel.height * -3f;
		
		stage.addActor(textGroup);
		textGroup.x = stage.centerX() - scoreLabel.width / 2;
		textGroup.y = 100f;
		
		friendImage = new Image("friend image", AssetsGame.getInstance().openfeintRegion);
		friendImage.x = stage.width()/4f;
		friendImage.y = stage.height()/6f;
		stage.addActor(friendImage);
	}
	
	public Vector2 getCenter(Vector2 centerOfBiggest, float relativeAngle, float radius )
	{
		return new Vector2(radius, 0).rotate(relativeAngle+90).add(centerOfBiggest);
	}
	
	public Image makeObstacle(Vector2 pPosition, Obstacle.Type pType, float containerWidth, float containerHeight) {
		Image obstacle = new Obstacle(pType);
		obstacle.color.set(0,.5f,1,1);
		
		obstacle.x = containerWidth / 2 + pPosition.x - obstacle.width / 2;
		obstacle.y = containerHeight / 2 + pPosition.y - obstacle.height / 2;
		
		return obstacle;
	}
	
	public boolean keyUp(int keycode) {
		OrthographicCamera camera = (OrthographicCamera) stage.getCamera();
		
		switch (keycode) {
		case Keys.COMMA:
			camera.zoom += 0.1f; 
			return true;
		case Keys.PERIOD:
			camera.zoom -= 0.1f;
			return true;
		case Keys.ENTER:
			startGame();
			return true;
		case Keys.LEFT:
			setRotationSmooth(selectedLevel-1);
			
			return true;
		case Keys.RIGHT:
			setRotationSmooth(selectedLevel+1);
		}
		
		return super.keyUp(keycode);
	}
	
	@Override
	public boolean touchUp(int x, int y, int pointer, int button)
	{
		Group selected = getClosestLevel(biggestGroup.rotation);
		rotateToSmooth( -selected.rotation );
		
		return super.touchUp(x, y, pointer, button);
	}

	private void startGame()
	{
		Level level = getLevel(selectedLevel);
		
		if (CentrifugeGame.activityCallback.isLite()) {
			if (level.identifier > LEVELS_AVAILABLE_IN_LITE_VERSION) {
				CentrifugeGame.activityCallback.askToPurchaseFullVersion();
				return;
			}
		}
		
		if ( level != null )
		{
			CentrifugeGame.instance.setScreen( new GameScreen(this, level) );
		}
	}

	@Override
	public boolean touchDown(int x, int y, int pointer)
	{
		return false;
	}

	@Override
	public boolean tap(int x, int y, int count)
	{
		Gdx.app.log("LevelSelectorScreen.tap", "x = " + x + ", y = " + y + ", count = " + count);
		if ( selectedLevel != 0 )
		{
			startGame();
			return true;
		}
		return false;
	}

	@Override
	public boolean longPress(int x, int y)
	{
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY)
	{
		return false;
	}

	@Override
	public boolean pan(int x, int y, int deltaX, int deltaY)
	{
		biggestGroup.rotation -= deltaX * 0.1f;
		highlightLevel();
		return true;
	}

	@Override
	public boolean zoom(float originalDistance, float currentDistance)
	{
		return false;
	}
	
}
