package com.brewengine.centrifuge;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.badlogic.gdx.scenes.scene2d.actors.Label;


public interface ActivityCallback {
	
	public boolean isLite();
	public void askToPurchaseFullVersion();
	public void executeLink(String string);
	public Map<String, String> buildDetails();
	public void flurryEvent(String eventId, HashMap<String, String> parameters);
	
	boolean isInvalid();
	void showScores();
	void getLevelScores(int level, Label scoreLabel, Label friendScoreLabel, Image friendImage);
	void postScore(int level, int score);
	void postStreak(int streak);
	void postNoHitter();
	void postBreakerBreaker();
	void post720();
	void postOutOfSight();

}
