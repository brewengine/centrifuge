package com.brewengine.centrifuge;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.Gdx;

/* AUTO GENERATED FILE */

public abstract class AssetsGameGenerated {

	public static final String GLOW = "glow";
	public static final String GOAL = "goal";
	public static final String OPENFEINT = "openfeint";
	public static final String ORIENTATION_FLAT0 = "orientationFlat0";
	public static final String ORIENTATION_FLAT1 = "orientationFlat1";
	public static final String ORIENTATION_FLAT2 = "orientationFlat2";
	public static final String ORIENTATION_LEFT = "orientationLeft";
	public static final String ORIENTATION_NORMAL = "orientationNormal";
	public static final String ORIENTATION_RIGHT = "orientationRight";
	public static final String PARTICLE = "particle";
	public static final String TEXT = "text";
	public static final String X = "x";

	public TextureAtlas textures;
	public TextureRegion glowRegion;
	public TextureRegion goalRegion;
	public TextureRegion openfeintRegion;
	public TextureRegion orientationFlat0Region;
	public TextureRegion orientationFlat1Region;
	public TextureRegion orientationFlat2Region;
	public TextureRegion orientationLeftRegion;
	public TextureRegion orientationNormalRegion;
	public TextureRegion orientationRightRegion;
	public TextureRegion particleRegion;
	public TextureRegion textRegion;
	public TextureRegion xRegion;

	public void load() {
		textures = new TextureAtlas(Gdx.files.internal("game/textures.txt"));

		glowRegion = textures.findRegion(GLOW);
		goalRegion = textures.findRegion(GOAL);
		openfeintRegion = textures.findRegion(OPENFEINT);
		orientationFlat0Region = textures.findRegion(ORIENTATION_FLAT0);
		orientationFlat1Region = textures.findRegion(ORIENTATION_FLAT1);
		orientationFlat2Region = textures.findRegion(ORIENTATION_FLAT2);
		orientationLeftRegion = textures.findRegion(ORIENTATION_LEFT);
		orientationNormalRegion = textures.findRegion(ORIENTATION_NORMAL);
		orientationRightRegion = textures.findRegion(ORIENTATION_RIGHT);
		particleRegion = textures.findRegion(PARTICLE);
		textRegion = textures.findRegion(TEXT);
		xRegion = textures.findRegion(X);

	}
}
