package com.brewengine.centrifuge.actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.FadeTo;
import com.badlogic.gdx.scenes.scene2d.actions.Forever;
import com.badlogic.gdx.scenes.scene2d.actions.Sequence;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.brewengine.centrifuge.AssetsGame;

public class Obstacle extends Image {

	public enum Type {
		Standard,
		Converted,
		Goal,
	}
	public Type type;
	
	public static int sequence;
	
	public Obstacle(Vector2 pPosition) {
		this();
		x = pPosition.x;
		y = pPosition.y;
	}
	
	public Obstacle() {
		this(Type.Standard);
	}
	
	public Obstacle(Type pType) {
		super("obstacle" + ++sequence, regionForType(pType));
		type = pType;
	}
	
	public void enableFade() {
		// have obstacles fade in/out together
				action(
					Forever.$(
						Sequence.$(
							FadeTo.$(0.5f, 0.5f),
							FadeTo.$(1.0f, 0.5f)
						) 
					)
				);
				
				// have obstacles have random fade in/out start times
//				action(
//					Delay.$(
//						Forever.$(
//							Sequence.$(
//									FadeTo.$(0.5f, 0.5f),
//									FadeTo.$(1.0f, 0.5f)
//								) 
//							),
//						MathUtils.random(0f, 1f)
//					)
//				);
	}

	public static TextureRegion regionForType(Type pType) {
		TextureRegion region = null;
		
		if (pType == Obstacle.Type.Standard || pType == Obstacle.Type.Converted) {
			region = AssetsGame.getInstance().particleRegion;
		} else if (pType == Obstacle.Type.Goal) {
			region = AssetsGame.getInstance().goalRegion;
		}
		
		return region;
	}

}
