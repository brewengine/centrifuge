package com.brewengine.centrifuge.actors;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.brewengine.centrifuge.CentrifugeGame;

public class LinkImage extends Image
{

	public LinkImage(String name, TextureRegion region)
	{
		super(name, region);
	}

	@Override
	public void touchUp (float x, float y, int pointer) {
		CentrifugeGame.activityCallback.executeLink("http://www.brewengine.com");
	}
}
