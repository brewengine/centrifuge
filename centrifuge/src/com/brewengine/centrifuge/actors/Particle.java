package com.brewengine.centrifuge.actors;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actors.Image;
import com.brewengine.centrifuge.AssetsGame;

public class Particle extends Image {

//	public static final Color COLOR_STANDARD  = new Color(56 / 255f,  194 / 255f, 230 / 255f, 1f);
	public static final Color COLOR_STANDARD  = new Color(Color.GREEN);
	public static final Color COLOR_REMOVE    = new Color(100 / 255f, 149 / 255f, 237 / 255f, 1f);
	public static final Color COLOR_CANCEROUS = new Color(1f, 1f, 0f, 1f);
	
	public enum Type
	{
		Standard,
		Remove,
		Double,
		Cancerous,
	}
	public Type type;
	
	public static int sequence;
	public ParticleEffect effect;
	
	private float timeElapsed = 0;
	
	public Particle(Type type) {
		super("particle" + ++sequence, AssetsGame.getInstance().particleRegion);
		this.type = type;
		
		effect = new ParticleEffect();
		effect.load(Gdx.files.internal("game/decay.p"), AssetsGame.getInstance().textures);
		
		switch (type) {
		case Remove:
			color.set(COLOR_REMOVE);
			break;
		case Cancerous:
			color.set(COLOR_CANCEROUS);
			break;
		case Standard:
			color.set(COLOR_STANDARD);
			break;
		}
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		timeElapsed += Gdx.graphics.getDeltaTime();
		
		if ( type == Type.Cancerous )
		{
			super.color.set((MathUtils.sinDeg(timeElapsed*360f)+1f)/4f+.5f,(MathUtils.sinDeg(timeElapsed*390f)+1f)/4f+.5f,(MathUtils.sinDeg(timeElapsed*370f)+1f)/4f+.3f,1);
		}
		super.draw(batch, parentAlpha);
	}

}
