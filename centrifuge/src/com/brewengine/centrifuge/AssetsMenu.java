package com.brewengine.centrifuge;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class AssetsMenu extends AssetsMenuGenerated {

	public TextureRegion[] patches;
	
	private static AssetsMenu instance = null;
	
	protected AssetsMenu() {
		// defeat instantiation
	}
	
	public static AssetsMenu getInstance() {
		if (instance == null) {
			instance = new AssetsMenu();
			instance.load();
		}
		return instance;
	}
	
	public void load() {
		super.load();
		
		patches = new TextureRegion[9];
		patches[NinePatch.TOP_LEFT]      = borderTopLeftRegion;
		patches[NinePatch.TOP_CENTER]    = borderTopCenterRegion;
		patches[NinePatch.TOP_RIGHT]     = borderTopRightRegion;
		patches[NinePatch.MIDDLE_LEFT]   = borderMiddleLeftRegion;
		patches[NinePatch.MIDDLE_CENTER] = borderMiddleCenterRegion;
		patches[NinePatch.MIDDLE_RIGHT]  = borderMiddleRightRegion;
		patches[NinePatch.BOTTOM_LEFT]   = borderBottomLeftRegion;
		patches[NinePatch.BOTTOM_CENTER] = borderBottomCenterRegion;
		patches[NinePatch.BOTTOM_RIGHT]  = borderBottomRightRegion;
		
		// configure texture filtering
		brewengineRegion.getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}
	
}
