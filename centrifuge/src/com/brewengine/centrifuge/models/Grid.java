package com.brewengine.centrifuge.models;

import com.badlogic.gdx.math.Vector2;

public class Grid {

	public Vector2 origin = new Vector2();
	public Vector2 spacing = new Vector2();
	
	public Grid(float pHorizontalSpacing, float pVerticalSpacing) {
		this(pHorizontalSpacing, pVerticalSpacing, 0, 0);
	}
	
	public Grid(float horizontalSpacing, float verticalSpacing, float originX, float originY) {
		spacing.set(horizontalSpacing, verticalSpacing);
		origin.set(originX, originY);
	}
	
}
