package com.brewengine.centrifuge.models;

import com.brewengine.centrifuge.actors.Particle.Type;

public class ParticleLoader {

	/**
	 * Time when the particle is to be created in seconds after game start.
	 */
	public float time;
	
	/**
	 * Direction that the particle should travel in degrees relative to
	 * 3-o'clock with counter-clockwise being positive.
	 */
	public float direction;

	public boolean isDeployed = false;

	public Type type;

	public ParticleLoader(float pDirection) {
		this(0L, pDirection);
	}

	public ParticleLoader(float pTime, float pDirection) {
		this(pTime, pDirection, Type.Standard);
	}

	public ParticleLoader(float pTime, float pDirection, Type type) {
		time = pTime;
		direction = pDirection;
		this.type = type;
	}
	
	
}
