package com.brewengine.centrifuge.models;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.ObjectMap;
import com.brewengine.centrifuge.actors.Obstacle;

public class ObstacleLoader implements Serializable {

	public Vector2 position;
	public Obstacle.Type type;
	
	public ObstacleLoader()
	{
		type = Obstacle.Type.Standard;
	}
	
	public ObstacleLoader(float pX, float pY) {
		this(new Vector2(pX, pY), Obstacle.Type.Standard);
	}
	
	public ObstacleLoader(Vector2 pPosition, Obstacle.Type pType) {
		position = pPosition;
		type = pType;
	}
	
	@Override
	public void write(Json json) {
		json.writeValue("x", position.x);
		json.writeValue("y", position.y);
		json.writeValue("t", type);
	}

	@Override
	public void read(Json json, ObjectMap<String, Object> jsonData)
	{
		position = new Vector2();
		position.x = json.readValue("x", Float.class, jsonData);
		position.y = json.readValue("y", Float.class, jsonData);
		type = json.readValue("t", Obstacle.Type.class, jsonData);
	}

}
