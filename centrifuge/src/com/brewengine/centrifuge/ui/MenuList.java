package com.brewengine.centrifuge.ui;

import com.badlogic.gdx.scenes.scene2d.ui.List;

public class MenuList extends List {

	public TouchListener touchListener;
	
	public MenuList(String[] items, ListStyle style, String name) {
		super(items, style, name);
	}
	
	public void setTouchListener (TouchListener listener) {
		this.touchListener = listener;
	}

	@Override
	public void touchUp (float x, float y, int pointer) {
		touchDown(x, y, pointer);
		
		if (touchListener != null) {
			touchListener.touchUp(this, getSelectedIndex(), getSelection());
		}
	}

	@Override
	public void touchDragged (float x, float y, int pointer) {
		touchDown(x, y, pointer);
	}
	
	public interface TouchListener {
		public void touchUp(List list, int selectedIndex, String selection);
	}

}
