package com.brewengine.centrifuge.groups;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.brewengine.centrifuge.actors.Particle;

public class RotatorGroup extends Group {

	protected final List<Particle> particles;
	protected final List<Particle> immutableParticles;
	
	public RotatorGroup() {
		this("rotator");
	}
	
	public RotatorGroup(String name) {
		super(name);
		particles = new ArrayList<Particle>();
		immutableParticles = Collections.unmodifiableList(particles);
	}
	
	public List<Particle> getParticles() {
		return immutableParticles;
	}
	
	@Override
	public void addActor(Actor actor) {
		if (actor instanceof Particle) {
			Particle particle = (Particle) actor;
			particles.add(particle);
		}
		
		super.addActor(actor);
	}
	
	@Override
	public void removeActor(Actor actor) {
		if (actor instanceof Particle) {
			Particle particle = (Particle) actor;
			particles.remove(particle);
		}
		
		super.removeActor(actor);
	}
	
}
