package com.brewengine.centrifuge;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.brewengine.centrifuge.helpers.SoundManager;
import com.brewengine.centrifuge.screens.menus.MainMenuScreen;

public class CentrifugeGame extends Game {
	
	public static CentrifugeGame instance;
	public static ActivityCallback activityCallback;
	
	public CentrifugeGame(ActivityCallback pActivityCallback) {
		activityCallback = pActivityCallback;
		instance = this;
	}
	
	@Override
	public void create() {
		Gdx.app.setLogLevel(Application.LOG_ERROR);
		Gdx.app.log("CentrifugeGame.create", "Centrifuge starting.");
		SoundManager.load();
		setScreen(new MainMenuScreen());
		SoundManager.music.play();
	}
	
	@Override
	public void dispose() {
		SoundManager.music.dispose();
		super.dispose();
	}

}
