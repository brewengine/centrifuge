package com.brewengine.centrifuge.levels;

import com.badlogic.gdx.math.Vector2;
import com.brewengine.centrifuge.actors.Obstacle.Type;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.models.ParticleLoader;

import com.brewengine.centrifuge.actors.Particle;

public class Level5 extends StringLoadedLevel
{

	String jsonLevel = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:409.5,y:10.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-409.5,y:10.5,t:Goal}]";
	
	@Override
	public void setup(float pWidth, float pHeight)
	{
		Vector2 vector2 = new Vector2(480/4,0);
		
		super.loadObstacles(jsonLevel);
		
		// The ring
		for (float i = 0; i < 360f; i+= 360f/27f )
		{
			obstacles.add(new ObstacleLoader( vector2.cpy().rotate(i), Type.Standard));
		}
		
		for (int j = 1; j < 9; j+= 4 )
		{
			for (float i = 0; i < 360; i+= 360f/27f )
			{
				particles.add(new ParticleLoader(j, i));
			}
		}
		
		for (int i = 0; i < 2; i++ )
		{
			particles.add(new ParticleLoader(9, i*180, Particle.Type.Remove));
			particles.add(new ParticleLoader(10, i*180, Particle.Type.Remove));
			particles.add(new ParticleLoader(11, i*180, Particle.Type.Remove));	
			particles.add(new ParticleLoader(12, i*180, Particle.Type.Remove));	
		}

		for (int i = 0; i < 70; i++ )
		{
			particles.add(new ParticleLoader(13+i, i*20, Particle.Type.Remove));
			particles.add(new ParticleLoader(13+i, i*90-i, Particle.Type.Standard));
//			particles.add(new ParticleLoader(13+i, i*120+i, Particle.Type.Standard));
			particles.add(new ParticleLoader(13+i, i*190, Particle.Type.Remove));	
		}
		
		
	}

}
