package com.brewengine.centrifuge.levels;

import com.badlogic.gdx.math.Vector2;
import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.actors.Obstacle.Type;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.models.ParticleLoader;

public class Level6 extends StringLoadedLevel
{

	String jsonLevel = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:409.5,y:10.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-409.5,y:10.5,t:Goal}]";
	
	@Override
	public void setup(float pWidth, float pHeight)
	{
		Vector2 vector2 = new Vector2(480/6,0);
		
		super.loadObstacles(jsonLevel);
		
		// The spiral
		for (int i = 0; i < 460; i+= 13 )
		{
			obstacles.add(new ObstacleLoader( vector2.cpy().add(i*.3f, 0).rotate(i), Type.Standard));
		}
		
		for (int i = 0; i < 20; i++ )
		{
			particles.add(new ParticleLoader(i*.1f, -i*20, Particle.Type.Standard));
		}
		
		for (int i = 0; i < 50; i++ )
		{
			particles.add(new ParticleLoader(6+i, i*20, Particle.Type.Remove));
			particles.add(new ParticleLoader(7+i, i*20 + 120, Particle.Type.Standard));
//			particles.add(new ParticleLoader(6+i, i*120+i, Particle.Type.Standard));
			particles.add(new ParticleLoader(8+i, i*20 + 240, Particle.Type.Remove));	
		}
		
	}

}
