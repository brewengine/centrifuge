package com.brewengine.centrifuge.levels;

import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.models.ParticleLoader;

public class Level9 extends StringLoadedLevel
{

	private String loadString = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:8.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-8.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:8.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-8.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:24.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-24.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:24.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-24.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:40.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-40.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:40.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-40.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:56.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-56.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:56.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-56.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:88.0,y:-32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-88.0,y:32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:88.0,y:32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-88.0,y:-32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:104.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-104.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:104.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-104.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-120.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-120.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:136.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-136.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:136.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-136.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:152.0,y:-128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-152.0,y:128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:152.0,y:128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-152.0,y:-128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-168.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-168.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:200.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-200.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:200.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-200.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:216.0,y:-128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-216.0,y:128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:216.0,y:128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-216.0,y:-128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:232.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-232.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:232.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-232.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:248.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-248.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:248.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-248.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-264.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-264.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:280.0,y:-32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:280.0,y:40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:296.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:296.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:-16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:328.0,y:-8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:328.0,y:64.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:344.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:344.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-280.0,y:-40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-296.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-328.0,y:-64.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-344.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-280.0,y:32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-296.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-328.0,y:8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-344.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:363.5,y:42.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-364.5,y:-41.5,t:Goal}]";

	@Override
	public void setup(float pWidth, float pHeight)
	{
		super.loadObstacles(loadString );
		
		particles.add(new ParticleLoader(0f, 0,       Particle.Type.Standard));
		particles.add(new ParticleLoader(1f, 0,       Particle.Type.Standard));
		
		particles.add(new ParticleLoader(6f, 180,       Particle.Type.Standard));
		particles.add(new ParticleLoader(9f, 0,       Particle.Type.Standard));
		
		particles.add(new ParticleLoader(11f, 0,       Particle.Type.Standard));
		particles.add(new ParticleLoader(12f, 0,       Particle.Type.Standard));
		particles.add(new ParticleLoader(12f, 180,       Particle.Type.Standard));
		
		particles.add(new ParticleLoader(20f, 10,       Particle.Type.Standard));
		particles.add(new ParticleLoader(20f, 190,       Particle.Type.Standard));
		
		particles.add(new ParticleLoader(28f, 0,       Particle.Type.Standard));
		particles.add(new ParticleLoader(28f, 180,       Particle.Type.Standard));
		particles.add(new ParticleLoader(29f, 0,       Particle.Type.Standard));
		particles.add(new ParticleLoader(29f, 180,       Particle.Type.Standard));
		
		particles.add(new ParticleLoader(30f, 0,       Particle.Type.Remove));
		particles.add(new ParticleLoader(30f, 180,       Particle.Type.Remove));
		particles.add(new ParticleLoader(31f, 0,       Particle.Type.Remove));
		particles.add(new ParticleLoader(31f, 180,       Particle.Type.Remove));
		particles.add(new ParticleLoader(32f, 0,       Particle.Type.Remove));
		particles.add(new ParticleLoader(32f, 180,       Particle.Type.Remove));
		particles.add(new ParticleLoader(33f, 0,       Particle.Type.Remove));
		particles.add(new ParticleLoader(33f, 180,       Particle.Type.Remove));
		
		particles.add(new ParticleLoader(40f, 10,       Particle.Type.Standard));
		particles.add(new ParticleLoader(40f, 190,       Particle.Type.Standard));
		particles.add(new ParticleLoader(48f, 20,       Particle.Type.Standard));
		particles.add(new ParticleLoader(48f, 200,       Particle.Type.Standard));
		particles.add(new ParticleLoader(56f, 30,       Particle.Type.Standard));
		particles.add(new ParticleLoader(56f, 210,       Particle.Type.Standard));
		particles.add(new ParticleLoader(64f, 40,       Particle.Type.Standard));
		particles.add(new ParticleLoader(64f, 250,       Particle.Type.Standard));
		
		
		
	}

}
