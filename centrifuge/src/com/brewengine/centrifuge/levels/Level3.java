package com.brewengine.centrifuge.levels;

import com.brewengine.centrifuge.models.ParticleLoader;

public class Level3 extends StringLoadedLevel
{

	String levelJson = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:389.5,y:3.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:389.5,y:-100.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:389.5,y:107.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:128.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:128.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:136.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:136.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:256.0,y:120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:256.0,y:-120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-152.0,y:128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-152.0,y:-128.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-144.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-144.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-152.0,y:80.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-152.0,y:-80.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-160.0,y:152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-160.0,y:-152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-160.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-160.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-376.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-384.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-384.0,y:-24.0,t:Standard}]";
	
	@Override
	public void setup(float pWidth, float pHeight) {
		
		super.loadObstacles(levelJson);
		
		for (int i = 0; i < 3; i++)
		{
			particles.add(new ParticleLoader(i*3, i*15));
		}

		for (int i = 0; i < 4; i++)
		{
			particles.add(new ParticleLoader(i*2.7f + 13f, i*20));
		}
		
		for (int i = 0; i < 5; i++)
		{
			particles.add(new ParticleLoader(i*2.2f + 26f, i*25));
		}

		for (int i = 0; i < 5; i++)
		{
			particles.add(new ParticleLoader(i*2.3f + 35f, i*35));
			particles.add(new ParticleLoader(i*2.3f + 35f, i*30));
		}
		
		for (int i = 0; i < 10; i++)
		{
			particles.add(new ParticleLoader(i*1.8f + 52f, i*35));
			particles.add(new ParticleLoader(i*1.8f + 52f, i*40));
			particles.add(new ParticleLoader(i*1.8f + 52f, i*45));
		}
	}

}
