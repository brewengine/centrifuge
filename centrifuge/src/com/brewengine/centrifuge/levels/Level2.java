package com.brewengine.centrifuge.levels;

import com.brewengine.centrifuge.actors.Particle.Type;
import com.brewengine.centrifuge.models.ParticleLoader;

public class Level2 extends StringLoadedLevel
{

	String levelJson = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-377.5,y:3.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:376.5,y:3.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-120.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:128.0,y:-32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-128.0,y:32.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:112.0,y:-64.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-112.0,y:64.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:208.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-208.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:200.0,y:120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-200.0,y:-120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:192.0,y:136.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:-136.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-264.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-248.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:248.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-232.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:232.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-280.0,y:184.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:280.0,y:-184.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-288.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:288.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-296.0,y:152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:296.0,y:-152.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:288.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-288.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:288.0,y:16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-288.0,y:16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:288.0,y:-16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-288.0,y:-16.0,t:Standard}]";

	@Override
	public void setup(float pWidth, float pHeight) {
		
		super.loadObstacles(levelJson);
		
		particles.add(new ParticleLoader(1, 0, Type.Cancerous));
		particles.add(new ParticleLoader(3, 90, Type.Standard));
		particles.add(new ParticleLoader(5, 90, Type.Standard));
		particles.add(new ParticleLoader(5, 250, Type.Standard));
		particles.add(new ParticleLoader(6, 110, Type.Standard));
		particles.add(new ParticleLoader(10, 180, Type.Standard));
		particles.add(new ParticleLoader(13, 180, Type.Standard));
		particles.add(new ParticleLoader(13, 75, Type.Cancerous));
		particles.add(new ParticleLoader(15, 0, Type.Standard));
		particles.add(new ParticleLoader(18, 0, Type.Standard));
		particles.add(new ParticleLoader(20, 45, Type.Cancerous));
		particles.add(new ParticleLoader(20, 80, Type.Standard));
		particles.add(new ParticleLoader(20, 145, Type.Standard));
		particles.add(new ParticleLoader(21, 245, Type.Standard));
		particles.add(new ParticleLoader(21, 60, Type.Cancerous));
		particles.add(new ParticleLoader(21, 20, Type.Standard));
		particles.add(new ParticleLoader(22, 180, Type.Standard));
		particles.add(new ParticleLoader(22, 300, Type.Cancerous));
		particles.add(new ParticleLoader(23, 0, Type.Standard));
		particles.add(new ParticleLoader(23, 170, Type.Cancerous));
		particles.add(new ParticleLoader(24, 270, Type.Standard));
		particles.add(new ParticleLoader(25, 330, Type.Standard));
		particles.add(new ParticleLoader(26, 10, Type.Standard));
		particles.add(new ParticleLoader(26, 80, Type.Cancerous));
		particles.add(new ParticleLoader(27, 10, Type.Standard));
		particles.add(new ParticleLoader(28, 200, Type.Cancerous));
	}

}
