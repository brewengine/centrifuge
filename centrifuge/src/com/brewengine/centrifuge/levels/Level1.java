package com.brewengine.centrifuge.levels;

import com.badlogic.gdx.math.Vector2;
import com.brewengine.centrifuge.actors.Obstacle;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.models.ParticleLoader;

public class Level1 extends Level {

	@Override
	public void setup(float pWidth, float pHeight) {
		
		float goalOffset = 20f;
		float offset = 75f;
		
		float top = pHeight / 2;
		float bottom = -pHeight / 2;
		float left = -pWidth / 2;
		float right = pWidth / 2;
		
		obstacles.add(new ObstacleLoader(right - offset, 0f)); // right
		obstacles.add(new ObstacleLoader(0f, top - offset)); // up
		obstacles.add(new ObstacleLoader(left + offset, 0f)); // left
		obstacles.add(new ObstacleLoader(0f, bottom + offset)); // down
		
		obstacles.add(new ObstacleLoader(new Vector2(-pWidth / 2 + goalOffset, 0f), Obstacle.Type.Goal)); // left
		obstacles.add(new ObstacleLoader(new Vector2(pWidth / 2 - goalOffset, 0f), Obstacle.Type.Goal)); // right
		
		for (int i = 0; i < 200; i++) {
			float time = i * 0.25f;
			float direction = i * 25f;
			
			particles.add(new ParticleLoader(time, direction));
		}
	}
	
}
