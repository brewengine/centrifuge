package com.brewengine.centrifuge.levels;

import java.io.Reader;
import java.io.StringReader;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.brewengine.centrifuge.models.ObstacleLoader;

public abstract class StringLoadedLevel extends Level
{
	
	public void loadObstacles(String loadString)
	{
		Array<ObstacleLoader> obstacles2 = new Array<ObstacleLoader>();
		
		Reader reader = new StringReader(loadString);
		Json json = new Json();
		obstacles2 = json.fromJson(Array.class, reader);
		
		obstacles.addAll(obstacles2);
	}
	
}
