package com.brewengine.centrifuge.levels;

import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.models.ParticleLoader;

public class Level10 extends StringLoadedLevel
{
	
	private String loadString = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:144.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:144.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-96.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-120.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-144.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-168.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-336.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-336.0,y:0.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-336.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-96.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-120.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-144.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-168.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:-48.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-240.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-264.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-288.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-240.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-264.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-288.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:-120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-336.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-360.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-384.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-408.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-336.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-360.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-384.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-408.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:144.0,y:120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:144.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:192.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:216.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:264.0,y:-216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:288.0,y:216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:288.0,y:-216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:-216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:336.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:336.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:360.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:360.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:384.0,y:192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:384.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:408.0,y:216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:408.0,y:-216.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:96.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:120.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:144.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:144.0,y:-120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:168.0,y:-96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:192.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:216.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:240.0,y:-192.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-192.0,y:96.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-216.0,y:-120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-216.0,y:-144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-216.0,y:120.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-216.0,y:144.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-383.5,y:75.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-383.5,y:-74.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-403.5,y:-2.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:292.5,y:120.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:318.5,y:55.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:318.5,y:-54.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:292.5,y:-119.5,t:Goal}]";

	@Override
	public void setup(float pWidth, float pHeight)
	{
		super.loadObstacles(loadString );
		int i = 1;
		line(i, 0);
		i+=7;
		wall(i, 90);
		i+=7;
		wall(i, 180);
		i+=7;
		line(i, 270);
		i+=3;
		line(i, 250);
		i+=3;
		line(i, 230);
		i+=7;
		particles.add(new ParticleLoader(i, 210,       Particle.Type.Remove));
		particles.add(new ParticleLoader(i, 30,       Particle.Type.Remove));
		i+=7;
		line(i, 270);
		i+=4;
		wall(i, 80);
		i+=4;
		line(i, 230);
		i+=4;
		wall(i, 70);
		
		// Hard ending!
		i+=7;
		wall(i, 90);
		i+=1;
		wall(i, 110);
	}

	void line(float startTime, float angle)
	{
		float increment = 0.25f;
		for (int i = 0; i < 5; i++)
		{
			particles.add(new ParticleLoader(startTime + i * increment, angle,
					Particle.Type.Standard));
		}
	}

	void wall(float time, float angle)
	{
		float increment = 3f;
		for (int i = 0; i < 5; i++)
		{
			particles.add(new ParticleLoader(time, angle + i * increment,
					Particle.Type.Standard));
		}
	}
	
}
