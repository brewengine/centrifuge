package com.brewengine.centrifuge.levels;

import com.brewengine.centrifuge.models.ParticleLoader;

public class Level4 extends StringLoadedLevel
{

	String levelJson = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-403.5,y:-165.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-403.5,y:166.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-403.5,y:68.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-403.5,y:-67.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:402.5,y:166.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:402.5,y:68.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:402.5,y:-165.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:402.5,y:-67.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:220.5,y:3.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-234.5,y:3.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:-8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:-8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:-40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:72.0,y:40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-72.0,y:-40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:104.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:88.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-88.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-88.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:88.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:72.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:40.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:24.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:-8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:184.0,y:-8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-184.0,y:8.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:296.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-296.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:296.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-296.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:328.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-328.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:328.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-328.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:344.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-344.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:344.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-344.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:360.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-360.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:360.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-360.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:280.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-280.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:280.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-280.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-8.0,y:-176.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:8.0,y:176.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-8.0,y:176.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:8.0,y:-176.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-24.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:24.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-24.0,y:168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:24.0,y:-168.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-40.0,y:-160.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:40.0,y:160.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-40.0,y:160.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:40.0,y:-160.0,t:Standard}]";
	
	@Override
	public void setup(float pWidth, float pHeight)
	{
		super.loadObstacles(levelJson);
		
		for (int i = 0; i < 3; i++)
		{
			particles.add(new ParticleLoader(i*3, i*15));
		}

		for (int i = 0; i < 4; i++)
		{
			particles.add(new ParticleLoader(i*2.7f + 13f, i*20));
		}
		
		for (int i = 0; i < 5; i++)
		{
			particles.add(new ParticleLoader(i*2.2f + 26f, i*25));
		}

		for (int i = 0; i < 5; i++)
		{
			particles.add(new ParticleLoader(i*2.3f + 35f, i*35));
			particles.add(new ParticleLoader(i*2.3f + 35f, i*30));
		}
		
		for (int i = 0; i < 10; i++)
		{
			particles.add(new ParticleLoader(i*1.8f + 52f, i*35));
			particles.add(new ParticleLoader(i*1.8f + 52f, i*40));
			particles.add(new ParticleLoader(i*1.8f + 52f, i*45));
		}
	}

}
