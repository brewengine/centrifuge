package com.brewengine.centrifuge.levels;

import com.brewengine.centrifuge.actors.Particle;
import com.brewengine.centrifuge.models.ParticleLoader;

public class Level7 extends StringLoadedLevel
{

	String jsonLevel = "[{class:com.brewengine.centrifuge.models.ObstacleLoader,x:176.0,y:136.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-176.0,y:-136.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:160.0,y:-56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-160.0,y:56.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:312.0,y:-176.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-312.0,y:176.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:40.0,y:80.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-40.0,y:-80.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-368.0,y:-16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:368.0,y:16.0,t:Standard},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:-305.5,y:-119.5,t:Goal},{class:com.brewengine.centrifuge.models.ObstacleLoader,x:305.5,y:120.5,t:Goal}]";
	
	@Override
	public void setup(float pWidth, float pHeight)
	{
		super.loadObstacles(jsonLevel);
		
		particles.add(new ParticleLoader(1, 0, Particle.Type.Standard));
		particles.add(new ParticleLoader(3, 90, Particle.Type.Standard));
		particles.add(new ParticleLoader(5, 34, Particle.Type.Standard));
		particles.add(new ParticleLoader(7, 245, Particle.Type.Standard));
		particles.add(new ParticleLoader(9, 318, Particle.Type.Standard));
		particles.add(new ParticleLoader(10, 22, Particle.Type.Standard));
		particles.add(new ParticleLoader(11, 1, Particle.Type.Standard));
		particles.add(new ParticleLoader(12, 197, Particle.Type.Standard));
		particles.add(new ParticleLoader(13, 199, Particle.Type.Standard));
		particles.add(new ParticleLoader(14, 224, Particle.Type.Standard));
		particles.add(new ParticleLoader(14.5f, 53, Particle.Type.Standard));
		particles.add(new ParticleLoader(14.5f, 67, Particle.Type.Standard));
		particles.add(new ParticleLoader(15, 54, Particle.Type.Standard));
		particles.add(new ParticleLoader(15, 178, Particle.Type.Standard));
		particles.add(new ParticleLoader(15, 322, Particle.Type.Standard));
		particles.add(new ParticleLoader(15, 45, Particle.Type.Standard));
		particles.add(new ParticleLoader(16, 198, Particle.Type.Standard));
		particles.add(new ParticleLoader(16, 267, Particle.Type.Standard));
		particles.add(new ParticleLoader(16, 22, Particle.Type.Standard));
		particles.add(new ParticleLoader(17, 354, Particle.Type.Standard));
		particles.add(new ParticleLoader(17, 34, Particle.Type.Standard));
		particles.add(new ParticleLoader(17.5f, 190, Particle.Type.Standard));
		particles.add(new ParticleLoader(17.75f, 64, Particle.Type.Standard));
		particles.add(new ParticleLoader(18, 274, Particle.Type.Standard));
		particles.add(new ParticleLoader(18.1f, 351, Particle.Type.Standard));
	}

}
