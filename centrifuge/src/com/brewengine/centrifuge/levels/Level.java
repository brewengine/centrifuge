package com.brewengine.centrifuge.levels;

import com.badlogic.gdx.utils.Array;
import com.brewengine.centrifuge.models.ObstacleLoader;
import com.brewengine.centrifuge.models.ParticleLoader;

public abstract class Level
{

	public int identifier = 0;

	public Array<ObstacleLoader> obstacles = new Array<ObstacleLoader>();
	public Array<ParticleLoader> particles = new Array<ParticleLoader>();

	public abstract void setup(float pWidth, float pHeight);

}
